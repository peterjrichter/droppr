/* 
 *  File: distribute_test.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *  
 *  MIT License
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 */

package utils

import (
	"fmt"
	"io"
	"io/fs"
	"os"
	"testing"
	"testing/fstest"

	"github.com/stretchr/testify/assert"
)
 
func TestDeleteEmpty(t *testing.T) {
	testCases := map[string]struct{
	 input []string
	 expected []string
	}{
		"delete all": {
			[]string{"", ""},
	 		[]string{},
		},
		"delete start, mid, end": {
			[]string{"", "something", "", "left", ""},
	 		[]string{"something", "left"},
		},
		"delete none": {
			[]string{"this", "is", "a", "test"},
	 		[]string{"this", "is", "a", "test"},
		},
	}

	for key, tc := range testCases {
		assert.Equal(t, tc.expected, Delete_Empty(tc.input), "Test Case: " + key)
	}
}
 
func TestRemoveDuplicateValues(t *testing.T) {
	testCases := map[string]struct{
	 input []string
	 expected []string
	}{
		"two dups": {
			[]string{"alpha", "bravo", "alpha", "bravo"},
	 		[]string{"alpha", "bravo"},
		},
		"many dups": {
			[]string{"alpha", "bravo", "alpha", "alpha", "alpha", "alpha"},
	 		[]string{"alpha", "bravo"},
		},
		"empty strings": {
			[]string{"this", "", "is", "", "a", "", "test"},
	 		[]string{"this", "", "is", "a", "test"},
		},
	}

	for key, tc := range testCases {
		assert.Equal(t, tc.expected, RemoveDuplicateValues(tc.input), "Test Case: " + key)
	}
}

func TestCopyFile(t *testing.T) {
	testCases := map[string]struct{
		openError error
		createError error
		copyError error
		statError error
		chmodError error
		expected error
	}{
		"happy path": {
			nil,
			nil,
			nil,
			nil,
			nil,
			nil,
		},
		"bad open": {
			fmt.Errorf("Mock Error on Open"),
			nil,
			nil,
			nil,
			nil,
			fmt.Errorf("Mock Error on Open"),
		},
		"bad create": {
			nil,
			fmt.Errorf("Mock Error on Create"),
			nil,
			nil,
			nil,
			fmt.Errorf("Mock Error on Create"),
		},
		"bad copy": {
			nil,
			nil,
			fmt.Errorf("Mock Error on Copy"),
			nil,
			nil,
			fmt.Errorf("Mock Error on Copy"),
		},
		"bad stat": {
			nil,
			nil,
			nil,
			fmt.Errorf("Mock Error on Stat"),
			nil,
			fmt.Errorf("Mock Error on Stat"),
		},
		"bad chmod": {
			nil,
			nil,
			nil,
			nil,
			fmt.Errorf("Mock Error on Chmod"),
			fmt.Errorf("Mock Error on Chmod"),
		},
	}

	for key, tc := range testCases {
		mockOpen := func(name string) (*os.File, error) {
			return nil, tc.openError
		}
		mockCreate := func(name string) (*os.File, error) {
			return nil, tc.createError
		}
		mockCopy := func(dst io.Writer, src io.Reader) (int64, error) {
			return 42, tc.copyError
		}
		mockStat := func(name string) (os.FileInfo, error) {
			fileinfo := fstest.MapFS {
				name: {
					Data: []byte("hello world"),
				},
			}
			stat, _ := fileinfo.Stat(name)
			return stat, tc.statError
		}
		mockChmod := func(name string, mode os.FileMode) error {
			return tc.chmodError
		}
		assert.Equal(t, tc.expected, copyFile("src", "dst", mockOpen, mockCreate,
			mockCopy, mockStat, mockChmod), "Test Case: " + key)
	}
}


func TestCopyDir(t *testing.T) {
	testCases := map[string]struct{
		statError []error
		mkdirError []error
		readdirError []error
		filesystem []fstest.MapFS
		copyfileError []error
		expected error
		expectedFilesCopied int
	}{
		"happy path": {
			[]error{nil, nil},
			[]error{nil, nil},
			[]error{nil, nil},
			[]fstest.MapFS{
				{
					"alpha": {},
					"beta": {Mode: os.ModeDir},
				},
				{
					"gamma": {},
					"delta": {},
				},
			},
			[]error{nil, nil, nil},
			nil,
			3,
		},
		"bad stat": {
			[]error{nil, fmt.Errorf("Error in stat")},
			[]error{nil, nil},
			[]error{nil, nil},
			[]fstest.MapFS{
				{
					"alpha": {},
					"beta": {Mode: os.ModeDir},
				},
				{
					"gamma": {},
					"delta": {},
				},
			},
			[]error{nil, nil, nil},
			fmt.Errorf("Error in stat"),
			1,
		},
		"bad mkdir": {
			[]error{nil, nil},
			[]error{nil, fmt.Errorf("Error in mkdir")},
			[]error{nil, nil},
			[]fstest.MapFS{
				{
					"alpha": {},
					"beta": {Mode: os.ModeDir},
				},
				{
					"gamma": {},
					"delta": {},
				},
			},
			[]error{nil, nil, nil},
			fmt.Errorf("Error in mkdir"),
			1,
		},
		"bad copyfile": {
			[]error{nil, nil},
			[]error{nil, nil},
			[]error{nil, nil},
			[]fstest.MapFS{
				{
					"alpha": {},
					"beta": {Mode: os.ModeDir},
				},
				{
					"gamma": {},
					"delta": {},
				},
			},
			[]error{nil, nil, fmt.Errorf("Error in copyfile")},
			fmt.Errorf("Error in copyfile"),
			3,
		},
		"bad readdir": {
			[]error{nil, nil},
			[]error{nil, nil},
			[]error{fmt.Errorf("Error in readdir"), nil},
			[]fstest.MapFS{
				{
					"alpha": {},
					"beta": {Mode: os.ModeDir},
				},
				{
					"gamma": {},
					"delta": {},
				},
			},
			[]error{nil, nil, nil},
			fmt.Errorf("Error in readdir"),
			0,
		},
	}

	for key, tc := range testCases {
		statCallCount := -1
		mockStat := func(name string) (os.FileInfo, error) {
			statCallCount++
			fileinfo := fstest.MapFS {
				name: {
					Data: []byte("hello world"),
				},
			}
			stat, _ := fileinfo.Stat(name)
			return stat, tc.statError[statCallCount]
		}

		mkdirCallCount := -1
		mockMkdir := func(path string, perm os.FileMode) error {
			mkdirCallCount++
			return tc.mkdirError[mkdirCallCount]
		}

		readdirCallCount := -1
		mockReadDir := func(name string) ([]os.DirEntry, error) {
			readdirCallCount++
			filsys := tc.filesystem[readdirCallCount]
			content := []os.DirEntry{}
			for name, _ := range filsys {
				fi, _ := filsys.Stat(name)
				content = append(content, fs.FileInfoToDirEntry(fi))
			}
			return content, tc.readdirError[readdirCallCount]
		}

		copyfileCallCount := -1
		mockCopyFile := func(src, dst string) error {
			copyfileCallCount++
			return tc.copyfileError[copyfileCallCount]
		}

		assert.Equal(t, tc.expected, copyDir("src", "dst", mockStat, mockMkdir,
			mockReadDir, mockCopyFile), "Return for Test Case: " + key)
		assert.Equal(t, tc.expectedFilesCopied, copyfileCallCount+1, "Call count for Test Case: " + key)
	}
}

func TestMergeMap(t *testing.T) {
	testCases := map[string]struct{
		originalMap map[string]interface{}
		updateMap map[string]interface{}
		expectedMap map[string]interface{}
	}{
		"nested good": {
			map[string]interface{}{"top": 
				map[string]interface{}{
					"a": "alpha",
					"b": "beta",
					"nested": map[string]interface{}{
						"d": "delta",
						"e": "epsilon",
					},
				},
			},
			map[string]interface{}{"top": 
				map[string]interface{}{
					"b": "bravo",
					"c": "charlie",
					"nested": map[string]interface{}{
						"e": "echo",
						"f": "foxtrot",
					},
				},
			 "bottom":
			 	map[string]interface{}{
				"y": "yankee",
				"z": "zulu",
			   },
			},
			map[string]interface{}{"top": 
				map[string]interface{}{
					"a": "alpha",
					"b": "bravo",
					"c": "charlie",
					"nested": map[string]interface{}{
						"d": "delta",
						"e": "echo",
						"f": "foxtrot",
					},
				},
			 "bottom":
			 	map[string]interface{}{
				"y": "yankee",
				"z": "zulu",
			   },
			},
		},
		"string to map": {
			map[string]interface{}{
				"a": "alpha",
				"b": "beta",
			},
			map[string]interface{}{
				"b": map[string]string{
					"e": "echo",
					"f": "foxtrot",
				},
				"c": "charlie",
			},
			map[string]interface{}{
				"a": "alpha",
				"b": map[string]string{
					"e": "echo",
					"f": "foxtrot",
				},
				"c": "charlie",
			},
		},
	}

	for key, tc := range testCases {

		MergeMap(tc.originalMap, tc.updateMap)
		assert.Equal(t, tc.expectedMap, tc.originalMap, "Return for Test Case: " + key)
	}
}

func TestVersionAtLeast(t *testing.T) {
	testCases := map[string]struct{
		testVer string
		minVer string
		expected bool
	}{
		"only two on min": {"1.2.3", "1.2", true},
		"ignore non-digit and following": {"1.2.0-alpha7", "1.2.3", false},
		"bad major": {"1.2.3", "3", false},
		"Better": {"1.4.3", "1.2.3", true},
	}

	for key, tc := range testCases {

		result := VersionAtLeast(tc.testVer, tc.minVer)
		assert.Equal(t, tc.expected, result, "Failed Test Case: " + key)
	}
}
