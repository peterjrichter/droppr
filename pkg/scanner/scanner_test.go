package scanner

import (
	"bytes"
	"io/ioutil"
	"os"
	"testing"
)

func TestReadBom(t *testing.T) {
	bom := `{
 "components": [
 {
 "purl": "pkg:github/CycloneDX/cyclonedx-go"
 }
 ]
 }`

	tmpfile, err := ioutil.TempFile("", "bom")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(tmpfile.Name())

	if _, err := tmpfile.WriteString(bom); err != nil {
		t.Fatal(err)
	}
	if err := tmpfile.Close(); err != nil {
		t.Fatal(err)
	}

	result := ReadBom(tmpfile.Name())

	if result == nil {
		t.Errorf("Expected result to be non-nil")
	}
}

func TestSetupConfig(t *testing.T) {
	// Test with valid input
	var buf bytes.Buffer
	err := setupConfig([]string{"docker", "git"}, &buf, 4)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
	if buf.Len() == 0 {
		t.Errorf("Expected config to be written to writer")
	}

	// Test with invalid repository type
	buf.Reset()
	err = setupConfig([]string{"invalid"}, &buf, 4)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
	if buf.Len() == 0 {
		t.Errorf("Expected config to be written to writer")
	}

	// Test with nil writer
	err = setupConfig([]string{"docker"}, nil, 4)
	if err == nil {
		t.Errorf("Expected an error, got nil")
	}

	// Test with empty repository list
	buf.Reset()
	err = setupConfig([]string{}, &buf, 4)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
	if buf.Len() == 0 {
		t.Errorf("Expected config to be written to writer")
	}

	// Test with negative number of workers
	buf.Reset()
	err = setupConfig([]string{"docker"}, &buf, -1)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
	if buf.Len() == 0 {
		t.Errorf("Expected config to be written to writer")
	}

	// Test with zero workers
	buf.Reset()
	err = setupConfig([]string{"docker"}, &buf, 0)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
	if buf.Len() == 0 {
		t.Errorf("Expected config to be written to writer")
	}
}

func TestSetupConfigRepoTypes(t *testing.T) {
	var buf bytes.Buffer

	// Test for nil writer
	err := setupConfig([]string{"docker"}, nil, 1)
	if err == nil || err.Error() != "Writer cannot be nil" {
		t.Errorf("Expected error for nil writer")
	}

	// Test for different repository types
	repoTypes := []string{"docker", "git", "rpm", "pypi", "maven", "generic", "helm", "apt", "deb"}
	for _, repoType := range repoTypes {
		buf.Reset()
		err := setupConfig([]string{repoType}, &buf, 1)
		if err != nil {
			t.Errorf("Unexpected error for repo type %s: %v", repoType, err)
		}
		if buf.Len() == 0 {
			t.Errorf("Expected config to be written to writer")
		}
	}

	// Test for default case
	buf.Reset()
	err = setupConfig([]string{"unknown"}, &buf, 1)
	if err != nil {
		t.Errorf("Unexpected error for default case: %v", err)
	}
	if buf.Len() == 0 {
		t.Errorf("Expected config to be written to writer")
	}
}

func TestSetDefaultType(t *testing.T) {
	testCases := []struct {
		pType string
	}{
		{"docker"},
		{"git"},
		{"rpm"},
		{"pypi"},
		{"maven"},
		{"generic"},
		{"helm"},
		{"apt"},
		{"deb"},
	}

	for _, tc := range testCases {
		t.Run(tc.pType, func(t *testing.T) {
			pkg := setDefaultType(tc.pType)
			if pkg.Purl_Type != tc.pType {
				t.Errorf("Expected Purl_Type to be %s, got %s", tc.pType, pkg.Purl_Type)
			}
		})
	}
}
