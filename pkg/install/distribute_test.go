/*
 *  File: distribute_test.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"fmt"
	"reflect"
	"strings"
	"testing"
	"testing/fstest"

	cdx "github.com/CycloneDX/cyclonedx-go"
	"github.com/stretchr/testify/assert"

	"gitlab.com/hoppr/droppr/pkg/configs"
)

func TestCheckHopprVer(t *testing.T) {
	testCases := map[string]struct {
		input    string
		expected error
	}{
		"happy path": {
			`some other stuff
Hoppr Version: 1.8.0
more stuff`,
			nil,
		},
		"old hoppr": {
			`some other stuff
Hoppr Version: 1.5.0
more stuff`,
			fmt.Errorf("Bundle built with Hoppr version 1.5.0, version 1.8.0 required"),
		},
		"new major release": {
			`Hoppr Version: 2.0.0
more stuff`,
			nil,
		},
		"no version": {
			`some other stuff
more stuff`,
			fmt.Errorf("Unable to determine Hoppr version from _run_data_ file in bundle"),
		},
	}

	for key, tc := range testCases {
		testdata := fstest.MapFS{
			"testfile": {
				Data: []byte(tc.input),
			},
		}
		fd, _ := testdata.Open("testfile")
		assert.Equal(t, tc.expected, checkHopprVersion(fd), "Test Case: "+key)
	}

}

func TestParsePurl(t *testing.T) {
	testCases := map[string]struct {
		input    string
		expected parsedPurl
	}{
		"full": {"pkg:generic/path/to/wherever/mything.ext@1.2.3?a=17&b=01#somesubpath/to/a/place",
			parsedPurl{"pkg:generic/path/to/wherever/mything.ext@1.2.3?a=17&b=01#somesubpath/to/a/place",
				"generic",
				"path/to/wherever",
				"mything.ext",
				"1.2.3",
				map[string]string{"a": "17", "b": "01"},
				"somesubpath/to/a/place",
			},
		},
		"no-subpath": {"pkg:generic/path/to/wherever/mything.ext@1.2.3?a=17&b=01",
			parsedPurl{"pkg:generic/path/to/wherever/mything.ext@1.2.3?a=17&b=01",
				"generic",
				"path/to/wherever",
				"mything.ext",
				"1.2.3",
				map[string]string{"a": "17", "b": "01"},
				"",
			},
		},
		"bad-qualifier": {"pkg:generic/path/to/wherever/mything.ext@1.2.3?a=17&b#somesubpath/to/a/place",
			parsedPurl{"pkg:generic/path/to/wherever/mything.ext@1.2.3?a=17&b#somesubpath/to/a/place",
				"generic",
				"path/to/wherever",
				"mything.ext",
				"1.2.3",
				map[string]string{"a": "17"},
				"somesubpath/to/a/place",
			},
		},
		"minimal": {"pkg:generic/mything.ext",
			parsedPurl{"pkg:generic/mything.ext",
				"generic",
				"",
				"mything.ext",
				"",
				map[string]string{},
				"",
			},
		},
		"no-pkg": {"generic/mything.ext",
			parsedPurl{"generic/mything.ext",
				"",
				"",
				"",
				"",
				map[string]string{},
				"",
			},
		},
		"no-filename": {"pkg:generic/@3.4.5",
			parsedPurl{"pkg:generic/@3.4.5",
				"generic",
				"",
				"",
				"3.4.5",
				map[string]string{},
				"",
			},
		},
		"just-subpath": {"pkg:generic/mything.ext#some/sort/of/subpath",
			parsedPurl{"pkg:generic/mything.ext#some/sort/of/subpath",
				"generic",
				"",
				"mything.ext",
				"",
				map[string]string{},
				"some/sort/of/subpath",
			},
		},
	}

	for key, tc := range testCases {
		assert.Equal(t, tc.expected, ParsePurl(tc.input), "Test Case: "+key)
	}
}

func TestDistribute(t *testing.T) {
	testCases := map[string]struct {
		purls         []string
		hopprVer      string
		expectedCalls int
		expected      error
	}{
		"happy path": {
			[]string{"pkg:maven/test/alpha", "pgk:pypi/test/beta"},
			"1.8.42",
			2,
			nil,
		},
		"garbage purl": {
			[]string{"pkg:garbage/test/omega", "pkg:maven/test/alpha", "pgk:pypi/test/beta"},
			"2.0",
			3,
			nil,
		},
		"old hoppr": {
			[]string{"pkg:maven/test/alpha", "pgk:pypi/test/beta"},
			"0.2.0",
			0,
			fmt.Errorf("Bundle built with Hoppr version 0.2.0, version 1.8.0 required"),
		},
		"worker failure": {
			[]string{"pkg:maven/test/alpha", "ERRpgk:pypi/test/beta"},
			"1.8.42",
			2,
			fmt.Errorf("1 of 2 components failed to distribute properly"),
		},
	}

	for key, tc := range testCases {
		workerCalled := 0
		mockWorkerFunc := func(basedir string, config *configs.DropprConfig, comp cdx.Component) Result {
			workerCalled++
			return Result{!strings.HasPrefix(comp.PackageURL, "ERR"), comp, "mock worker result"}
		}

		mockReadBom := func(name string) *cdx.BOM {
			bom := new(cdx.BOM)
			var comps []cdx.Component
			for _, p := range tc.purls {
				c := testComponent(p)
				comps = append(comps, c)
			}
			bom.Components = &comps

			return bom
		}

		fileinfo := fstest.MapFS{
			"rundata": {
				Data: []byte("Hoppr Version: " + tc.hopprVer),
			},
		}

		rundata, _ := fileinfo.Open("rundata")
		config := &configs.DropprConfig{Num_Workers: 10}
		actual := distribute("BASEDIR", config, rundata, mockReadBom, mockWorkerFunc)

		assert.Equal(t, tc.expected, actual, "Error return for Test Case: "+key)
		assert.Equal(t, tc.expectedCalls, workerCalled, "Worker Call count for Test Case: "+key)
	}
}

func TestSelectDistributor(t *testing.T) {
	testCases := map[string]struct {
		purl         string
		expectedType string
	}{
		"docker": {
			"pkg:docker/test/alpha",
			"*install.dockerDist",
		},
		"maven": {
			"pkg:maven/test/alpha",
			"*install.mavenDist",
		},
		"pypi": {
			"pkg:pypi/test/alpha",
			"*install.pypiDist",
		},
		"helm": {
			"pkg:helm/test/alpha",
			"*install.helmDist",
		},
		"garbage": {
			"pkg:garbage/test/omega",
			"",
		},
		"rpm": {
			"pkg:rpm/test/alpha",
			"*install.rpmDist",
		},
		"git": {
			"pkg:git/test/alpha",
			"*install.gitDist",
		},
		"generic": {
			"pkg:generic/test/alpha",
			"*install.rawDist",
		},
	}

	for key, tc := range testCases {
		actual := selectDistributor("BASEDIR", nil, testComponent(tc.purl))

		if tc.expectedType == "" {
			assert.Nil(t, actual, "Expected nil return")
		} else {
			assert.Equal(t, tc.expectedType, reflect.TypeOf(actual).String(), "Return Type for Test Case: "+key)
		}
	}
}

type mockDist struct {
	baseDist
	getRepoReturnConfig  *configs.PackageType
	getRepoReturnError   error
	installLocalReturn   Result
	installNexusReturn   Result
	installFilesysReturn Result
}

func (self *mockDist) GetRepoConfig(comp cdx.Component) (*configs.PackageType, error) {
	return self.getRepoReturnConfig, self.getRepoReturnError
}

func (self *mockDist) InstallLocal(comp cdx.Component) Result {
	self.installLocalReturn.Component = comp
	return self.installLocalReturn
}

func (self *mockDist) InstallNexus(comp cdx.Component) Result {
	self.installNexusReturn.Component = comp
	return self.installNexusReturn
}

func (self *mockDist) InstallFilesys(comp cdx.Component) Result {
	if self.purlType == "PANIC" {
		panic("Test panic in mockDist")
	}
	self.installFilesysReturn.Component = comp
	return self.installFilesysReturn
}

func TestProcessComponent(t *testing.T) {
	placeholderComp := testComponent("testPurl")

	testCases := map[string]struct {
		dist     mockDist
		expected Result
	}{
		"filesys success": {
			mockDist{
				*NewBaseDist("maven", nil, "BASEDIR"),
				&configs.PackageType{Target_Type: "filesys"}, nil,
				Result{true, placeholderComp, ""}, Result{true, placeholderComp, ""}, Result{true, placeholderComp, ""},
			},
			Result{true, placeholderComp, ""},
		},
		"nexus success": {
			mockDist{
				*NewBaseDist("maven", nil, "BASEDIR"),
				&configs.PackageType{Target_Type: "nexus"}, nil,
				Result{true, placeholderComp, ""}, Result{true, placeholderComp, ""}, Result{true, placeholderComp, ""},
			},
			Result{true, placeholderComp, ""},
		},
		"local fail": {
			mockDist{
				*NewBaseDist("maven", nil, "BASEDIR"),
				&configs.PackageType{Target_Type: "local"}, nil,
				Result{false, placeholderComp, ""}, Result{true, placeholderComp, ""}, Result{true, placeholderComp, ""},
			},
			Result{false, placeholderComp, ""},
		},
		"bad target type": {
			mockDist{
				*NewBaseDist("maven", nil, "BASEDIR"),
				&configs.PackageType{Target_Type: "garbage"}, nil,
				Result{true, placeholderComp, ""}, Result{true, placeholderComp, ""}, Result{true, placeholderComp, ""},
			},
			Result{false, placeholderComp, "Invalid transfer type in config: garbage"},
		},
		"getRepo error": {
			mockDist{
				*NewBaseDist("maven", nil, "BASEDIR"),
				&configs.PackageType{Target_Type: "filesys"}, fmt.Errorf("Mock error"),
				Result{true, placeholderComp, ""}, Result{true, placeholderComp, ""}, Result{true, placeholderComp, ""},
			},
			Result{false, placeholderComp, "Error selecting repo for pkg:maven/some/thing: Mock error"},
		},
		"force panic": {
			mockDist{
				*NewBaseDist("PANIC", nil, "BASEDIR"),
				&configs.PackageType{Target_Type: "filesys"}, nil,
				Result{true, placeholderComp, ""}, Result{true, placeholderComp, ""}, Result{true, placeholderComp, ""},
			},
			Result{false, placeholderComp, "Unexpected error processing 'pkg:maven/some/thing': Test panic in mockDist"},
		},
	}

	comp := testComponent("pkg:maven/some/thing")
	for key, tc := range testCases {
		actual := processComponent(&tc.dist, comp)
		tc.expected.Component = comp

		assert.Equal(t, tc.expected, actual, "Test Case: "+key)
	}
}
