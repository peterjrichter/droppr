/* 
 *  File: docker_dist_test.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *  
 *  MIT License
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 */

 package install

 import (
	 "bytes"
	 "context"
	 "fmt"
	 "io"
	 "net/http"
	 "net/http/httptest"
	 "os"
	 "strings"
	 "testing"
 
	 dockerTypes "github.com/docker/docker/api/types"
	 "github.com/google/go-containerregistry/pkg/crane"
	 v1 "github.com/google/go-containerregistry/pkg/v1"
	 "github.com/stretchr/testify/assert"
 
	 "gitlab.com/hoppr/droppr/pkg/nexus"
 )

func TestDockerCheckNexusRepository(t *testing.T) {
	testCases := map[string]struct{
		suppliedDockerUrl string
		repoName string
		getRepoRc int
		getRepoJson string
		checkRealmRc int
		createRepoRc int
		expected error
		}{
			"got repo": {
				"",
				"test-docker-repo",
				200,
				"[{\"name\": \"test-docker-repo\", \"format\": \"docker\", \"type\": \"hosted\", \"docker\": {\"httpPort\": 5000}}]",
				200,
				200,
				nil,
			},
			"bad repo name": {
				"",
				"Test-docker-repo",
				200,
				"[{\"name\": \"test-docker-repo\", \"format\": \"docker\", \"type\": \"hosted\", \"docker\": {\"httpPort\": 5000}}]",
				200,
				200,
				fmt.Errorf("'Test-docker-repo' is not a valid Docker repository name, must be all lower case"),
			},
			"invalid repo": {
				"",
				"test-docker-repo",
				200,
				"[{\"name\": \"test-docker-repo\", \"format\": \"maven2\", \"type\": \"hosted\", \"docker\": {\"httpPort\": 5000}}]",
				200,
				200,
				fmt.Errorf("Repository test-docker-repo has format 'maven2', format 'docker' requested"),
			},
			"bad http port": {
				"",
				"test-docker-repo",
				200,
				"[{\"name\": \"test-docker-repo\", \"format\": \"docker\", \"type\": \"hosted\", \"docker\": {\"httpPort\": 5001}}]",
				200,
				200,
				fmt.Errorf("Repository 'test-docker-repo' already exists, using HTTP port 5001, which is not the specified docker port 5000"),
			},
			"bad https port": {
				"",
				"test-docker-repo",
				200,
				"[{\"name\": \"test-docker-repo\", \"format\": \"docker\", \"type\": \"hosted\", \"docker\": {\"httpsPort\": 5001}}]",
				200,
				200,
				fmt.Errorf("Repository 'test-docker-repo' already exists, but has no HTTP port assigned.  Requested port is 5000"),
			},
			"create repo": {
				"",
				"test-docker-repo",
				200,
				"[{\"name\": \"good-repo\", \"format\": \"docker\", \"type\": \"hosted\", \"docker\": {\"httpPort\": 5000}}]",
				200,
				200,
				nil,
			},
			"get repo failure": {
				"",
				"test-docker-repo",
				401,
				"[{\"name\": \"good-repo\", \"format\": \"docker\", \"type\": \"hosted\", \"docker\": {\"httpPort\": 5000}}]",
				200,
				200,
				fmt.Errorf("Error response from HTTP call: 401 Unauthorized"),
			},
			"set realm failure": {
				"",
				"test-docker-repo",
				200,
				"[{\"name\": \"good-repo\", \"format\": \"docker\", \"type\": \"hosted\", \"docker\": {\"httpPort\": 5000}}]",
				401,
				200,
				fmt.Errorf("Error response from get realms HTTP call: 401 Unauthorized"),
			},
			"create repo failure": {
				"",
				"test-docker-repo",
				200,
				"[{\"name\": \"good-repo\", \"format\": \"docker\", \"type\": \"hosted\", \"docker\": {\"httpPort\": 5000}}]",
				200,
				401,
				fmt.Errorf("Error response from HTTP call to build repository: 401 Unauthorized"),
			},
			"docker url supplied": {
				"http://my.docker.com",
				"test-docker-repo",
				401,
				"[{\"name\": \"good-repo\", \"format\": \"docker\", \"type\": \"hosted\", \"docker\": {\"httpPort\": 5000}}]",
				401,
				401,
				nil,
			},
		}
		
	for key, tc := range testCases {
		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if strings.HasSuffix(fmt.Sprint(r.URL), "/hosted") {
				w.WriteHeader(tc.createRepoRc)
			} else if strings.HasSuffix(fmt.Sprint(r.URL), "/realms/active") {
				w.WriteHeader(tc.checkRealmRc)
				w.Write([]byte("[\"DockerToken\"]"))
			} else {
				w.WriteHeader(tc.getRepoRc)
				w.Write([]byte(tc.getRepoJson))
			}
		})

		server := httptest.NewServer(handler)
		defer server.Close()
	 
		dist := dockerDist{*NewBaseDist("docker", testConfig("nexus", server.URL), "basedirectory")}

		testNexus := nexus.Server{Url: server.URL, Docker_Port: 5000, Docker_Url: tc.suppliedDockerUrl}
	
		err := dist.CheckNexusRepository(testNexus, tc.repoName)

		assert.Equal(t, fmt.Sprint(tc.expected), fmt.Sprint(err), "Error mis-match, Test Case: " + key)
	}
}

func TestInstallDockerNexus(t *testing.T) {
	placeholderComp := testComponent("testPurl")
	testCases := map[string]struct{
		nexusUrl string
		loadError error
		pushError error
		expected Result
	   }{
		"success http": {"http://testnexus.com", nil, nil, Result{true, placeholderComp, ""} },
		"success https": {"https://testnexus.com", nil, nil, Result{true, placeholderComp, ""} },
		"bad load": {"http://testnexus.com", fmt.Errorf("Load failed"), nil, Result{false, placeholderComp, "Unable to load docker image from tarball: Load failed"} },
		"bad push": {"http://testnexus.com", nil, fmt.Errorf("Push failed"), Result{false, placeholderComp, "Push failed"} },
		}   

	for key, tc := range testCases {
		mockLoad := func(path string, opt ...crane.Option) (v1.Image, error) {
			img, _ := crane.Image(map[string][]byte{})
			return img, tc.loadError
		}
		mockPush := func(img v1.Image, dst string, opt ...crane.Option) error {
			return tc.pushError
		}

		testNexus := nexus.Server{Url: tc.nexusUrl, Docker_Port: 5000}

		dist := dockerDist{*NewBaseDist("docker", testConfig("nexus", "./loc"), "base_directory")}
		actual := dist.installNexus(placeholderComp, testNexus, mockLoad, mockPush)

		assert.Equal(t, tc.expected, actual, "Error, Test Case: " + key)
	}
}

func TestInstallDockerFilesys(t *testing.T) {
	placeholderComp := testComponent("testPurl")
	testCases := map[string]struct{
		purlType string
		mkdir_result error
		copy_result error
		expected Result
	   }{
		"success": {"docker", nil, nil, Result{true, placeholderComp, ""} },
		"purl-not-found": {"badPurl", nil, nil, 
			Result{false, placeholderComp, "Error installing pkg:badPurl/filename@1.2.3 component to local file system: Unable to locate configuration for Purl type badPurl matching repository http://my-repo"} },
		"bad-mkdir": {"docker", fmt.Errorf("Mkdir failed"), nil, 
			Result{false, placeholderComp, "Unable to create directory: ./loc4/the/collection/dir"} },
		"bad-copy": {"docker", nil, fmt.Errorf("Copy failed"), 
			Result{false, placeholderComp, "Unable to copy from base_directory/the/collection/dir/filename_1.2.3 to ./loc4/the/collection/dir/filename_1.2.3, Copy failed."} },
		}   

	for key, tc := range testCases {
		mockMkdir := func(path string, perm os.FileMode) error {
			return tc.mkdir_result
		}

		mockCopy := func(string, string) error {
			return tc.copy_result
		}

		component := testComponent("pkg:" + tc.purlType + "/filename@1.2.3")
		dist := dockerDist{*NewBaseDist(tc.purlType, testConfig("filesys", "./loc"), "base_directory")}
		actual := dist.installFilesys(component, mockMkdir, mockCopy)
		tc.expected.Component = component

		assert.Equal(t, tc.expected, actual, "Error, Test Case: " + key)
	}
}

func TestInstallDockerLocal(t *testing.T) {
	placeholderComp := testComponent("testPurl")
	testCases := map[string]struct{
		loadResultBody string
		loadResultJson bool
		loadError error
		tagError error
		expected Result
	   }{
		"success": {"\"Loaded image: sha256:1701abcdef\"", false, nil, nil, Result{true, placeholderComp, ""} },
		"successJson": {"[\"Loaded image: sha256:1701abcdef\"]", true, nil, nil, Result{true, placeholderComp, ""} },
		"bad image id": {"\"No Image ID Here\"", false, nil, nil, Result{false, placeholderComp, "Unable to determine loaded image id"} },
		"load error": {"\"Loaded image: sha256:1701abcdef\"", false, fmt.Errorf("Test Error"), nil, Result{false, placeholderComp, "Test Error"} },
		"tag error": {"\"Loaded image: sha256:1701abcdef\"", false, nil, fmt.Errorf("Tag Error"), Result{false, placeholderComp, "Tag Error"} },
		}   

	for key, tc := range testCases {
		mockImgLoad := func(ctx context.Context, input io.Reader, quiet bool) (dockerTypes.ImageLoadResponse, error) {
			resp := dockerTypes.ImageLoadResponse{}
			resp.JSON = tc.loadResultJson
			resp.Body = io.NopCloser(bytes.NewReader([]byte(tc.loadResultBody)))
			return resp, tc.loadError
		}

		mockImageTag := func(ctx context.Context, imageID, ref string) error {
			return tc.tagError
		}

		dist := dockerDist{*NewBaseDist("purlType", testConfig("local", "./loc"), "base_directory")}
		reader := bytes.NewReader([]byte(""))
		actual := dist.installLocal(placeholderComp, reader, mockImgLoad, mockImageTag)

		assert.Equal(t, tc.expected, actual, "Error, Test Case: " + key)
	}
}

func TestGetDockerFilename(t *testing.T) {
	testCases := map[string]struct{
		collector string
		expected string
	   }{
		"old": {"TestColl:1.8.0", "enterprise_17.0.1"},
		"new": {"TestColl:2", "enterprise@17.0.1"},
		"no version": {"TestColl", "enterprise_17.0.1"},
		}   

	for key, tc := range testCases {
		component := testComponent("pkg:docker/stuff/enterprise@17.0.1")
		for i, p := range *component.Properties {
			if p.Name == collectionPlugin {
				(*component.Properties)[i].Value = tc.collector
			}
		}

		dist := dockerDist{*NewBaseDist("purlType", testConfig("local", "./loc"), "base_directory")}
		actual := dist.getFileName(component)

		assert.Equal(t, tc.expected, actual, "Error, Test Case: " + key)
	}
}
