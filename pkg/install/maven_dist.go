/* 
 *  File: distribute.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *  
 *  MIT License
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 */

 package install

 import (
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"

	cdx "github.com/CycloneDX/cyclonedx-go"
	"gitlab.com/hoppr/droppr/pkg/nexus"
	"gitlab.com/hoppr/droppr/pkg/utils"
 )

 type mavenDist struct {
	baseDist
 }

 type coordinates struct {
	groupId string
	artifactId string
	version string
	extension string
 }

 func (self *mavenDist) InstallLocal(comp cdx.Component) Result {
	return self.installLocal(comp, os.ReadDir)
 }

 func (self *mavenDist) installLocal(
	comp cdx.Component,
	readDirFunc func(filename string) ([]os.DirEntry, error),
 	) Result {

	// Maven local install process from https://maven.apache.org/guides/mini/guide-3rd-party-jars-local.html

	dir := self.baseDir + string(os.PathSeparator) + getCollectionData(comp)[collectionDirectory]

	files, err := readDirFunc(dir)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	processed := map[string]bool{}

	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		return Result{false, comp, fmt.Sprintf("Error installing %s component locally: %s", comp.PackageURL, err)}
	}
	if len(repoConfig.Package_Manager_Command) == 0 || repoConfig.Package_Manager_Command[0] == "" {
		repoConfig.Package_Manager_Command = []string{"mvn"}
	}

	// Process all files associated with pom files
	
	for _, f := range(files) {
		mavenCoords := getCoordinates(dir, f.Name())
		if mavenCoords.extension != "pom" {
			continue
		}

		pomfn := dir + string(os.PathSeparator) + f.Name()

		command := append([]string{}, repoConfig.Package_Manager_Command...)
		command = append(command, "install:install-file", "-DpomFile="+pomfn)
		processed[f.Name()] = true

		// Look for a corresponding jar file and add it to the command, if needed
		jarfn := mavenCoords.artifactId + "_" + mavenCoords.version + "."
		for _, otherFile := range(files) {
			if strings.HasPrefix(otherFile.Name(), jarfn) && otherFile.Name() != f.Name() {
				command = append(command, "-Dfile="+dir + string(os.PathSeparator) + otherFile.Name())
				processed[otherFile.Name()] = true
			}
		}

		output, err := runCommand(command)
		self.Log().Println("Command output:\n     " + strings.ReplaceAll(output, "\n", "\n     "))
		if err != nil {
			return Result{false, comp, err.Error()}
		}
	}

	// Process all remaining files
	
	for _, f := range(files) {
		if _, gotit := processed[f.Name()]; gotit {
			continue
		}

		mavenCoords := getCoordinates(dir, f.Name())
		fqfn := dir + string(os.PathSeparator) + f.Name()

		command := append([]string{}, repoConfig.Package_Manager_Command...)
		command = append(command, "install:install-file",
			"-Dfile=" + fqfn,
			"-DgroupId=" + mavenCoords.groupId,
			"-DartifactId=" + mavenCoords.artifactId,
			"-Dversion=" + mavenCoords.version,
			"-Dpackaging=" + mavenCoords.extension)

		output, err := runCommand(command)
		self.Log().Println("Command output: " + output)
		if err != nil {
			return Result{false, comp, err.Error()}
		}
	}

	return Result{true, comp, ""}
 }

 func (self *mavenDist) InstallNexus(comp cdx.Component) Result {
	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		msg := fmt.Sprintf("Error installing %s component to Nexus: %s", comp.PackageURL, err)
		self.Log().Error(msg)
		return Result{false, comp, msg}
	}

	serverUrl, repoName := nexus.ParseLocation(repoConfig.Target_Location, "droppr_maven")

	targetNexus := nexus.Server{
		Url: serverUrl, 
		Username: repoConfig.Username, 
		Password: repoConfig.Password,
	}

	err = self.CheckNexusRepository(targetNexus, repoName)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	return self.installNexus(comp, targetNexus, repoName, os.ReadDir, utils.OpenFileAsReader)
 }

 func (self *mavenDist) installNexus(
	comp cdx.Component, 
	targetNexus nexus.Server, 
	repoName string,
	readDirFunc func(name string) ([]os.DirEntry, error),
	openFunc func(name string) (io.Reader, error),
	) Result {
	
	dir := self.baseDir + string(os.PathSeparator) + getCollectionData(comp)[collectionDirectory]
	files, err := readDirFunc(dir)
	if err != nil {
		return Result{false, comp, err.Error()}
	}


	for _, f := range(files) {
		mavenCoords := getCoordinates(dir, f.Name())

		fptr, err := openFunc(dir + string(os.PathSeparator) + f.Name())
		if err != nil {
			return Result{false, comp, err.Error()}
		}

		data := map[string]interface{}{
			"maven2.groupId": mavenCoords.groupId,
			"maven2.artifactId": mavenCoords.artifactId,
			"maven2.version": mavenCoords.version,
			"maven2.asset1.extension": mavenCoords.extension,
		}

		err = targetNexus.Upload(f.Name(), "maven2.asset1", fptr, repoName, data)
		if err != nil {
			return Result{false, comp, err.Error()}
		}
	}

	
	return Result{true, comp, ""}
 }

 func (self *mavenDist) CheckNexusRepository(targetNexus nexus.Server, repoName string) error {
	targetNexus.GetLock().Lock()
	defer targetNexus.GetLock().Unlock()

	repo, err := targetNexus.GetRepository(repoName)
	if err != nil {
		return err
	}

	additionalParams := map[string]interface{}{
		"maven": map[string]string{
			"versionPolicy": "MIXED",
			"layoutPolicy": "STRICT",
		},
	}

	if repo != nil {
		err := self.ValidateNexusRepository(repo, "maven2", additionalParams)
		if err != nil {
			return err
		}
	} else {
		err := targetNexus.CreateRepository(repoName, "maven", "maven2", additionalParams)
		if err != nil {
			return err
		}
	}

	return nil
 }

 func getCoordinates(dir string, fileName string) coordinates {
	// regexp to extract artifactId, version, and extension from the file name
	re := regexp.MustCompile(`(.*?)_(\d+(?:\.\d+)*)\.(.*)`)

	coord := coordinates{}

	pathParts := strings.Split(dir, string(os.PathSeparator))
	coord.groupId = pathParts[len(pathParts) - 1]

	matches := re.FindStringSubmatch(fileName)
	coord.artifactId = matches[1]
	coord.version = matches[2]
	coord.extension = matches[3]

	return coord
 }