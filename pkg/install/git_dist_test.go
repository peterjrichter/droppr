/*
 *  File: git_dist_test.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/hoppr/droppr/pkg/configs"
	"gitlab.com/hoppr/droppr/pkg/nexus"
)

// NEEDS REWORK
func TestGitInstallNexus(t *testing.T) {
	placeholderComp := testComponent("testPurl")
	testCases := map[string]struct {
		fileNames []string
		uploadRc  []int
		openError bool
		expected  Result
		}{
			"good": {
				[]string{"alpha_1.2.3.pom", "beta_4.5.jar", "gamma_6.7.8.jar"},
				[]int{200, 200, 200},
				false,
				Result{true, placeholderComp, ""},
			},
			"upload failure": {
				[]string{"alpha_1.2.3.pom", "beta_4.5.jar", "gamma_6.7.8.jar"},
				[]int{200, 200, 400},
				false,
				Result{false, placeholderComp, "Error response from upload HTTP call: 400 Bad Request"},
			},
			"read error": {
				[]string{"alpha_1.2.3.pom", "beta_4.5.jar", "gamma_6.7.8.jar"},
				[]int{200, 200, 200},
				true,
				Result{false, placeholderComp, "Mock Open Error"},
			},
		}
		
	for key, tc := range testCases {
		serverCallIndex := 0
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(tc.uploadRc[serverCallIndex])
			serverCallIndex++
		}))
		defer server.Close()
	 
		tempDir, _ := os.MkdirTemp("/tmp", "git_test")
		defer os.RemoveAll(tempDir)

		dist := gitDist{*NewBaseDist("git", testConfig("nexus", server.URL), tempDir)}
		comp := testComponent("pkg:git/name/space/TestGit@1.2.3")

		srcDir := tempDir + string(os.PathSeparator) + getCollectionData(comp)[collectionDirectory]
		os.MkdirAll(srcDir, os.ModePerm)
		for _, f := range tc.fileNames {
			os.Create(srcDir + string(os.PathSeparator) + f)
		}
		testNexus := nexus.Server{Url: server.URL}
	
		mockOpen := func(name string) (io.Reader, error) {
			if tc.openError {
				return bytes.NewReader([]byte("")), fmt.Errorf("Mock Open Error")
			}
			return bytes.NewReader([]byte("Data from file " + name)), nil
		}
	
		result := dist.installNexus(comp, testNexus, "test_git_repo", mockOpen)

		assert.Equal(t, tc.expected.Success, result.Success, "Result Status mis-match, Test Case: " + key)
		assert.Equal(t, comp, result.Component, "Component mis-match, Test Case: " + key)
		assert.Equal(t, tc.expected.Message, result.Message, "Message mis-match, Test Case: " + key)
	}
}

func TestGitCheckNexusRepository(t *testing.T) {
	testCases := map[string]struct {
		getRepoRc    int
		getRepoJson  string
		createRepoRc int
		expected     error
		}{
			"got repo": {
				200,
				"[{\"name\": \"test-git-repo\", \"format\": \"raw\", \"type\": \"hosted\"}]",
				200,
				nil,
			},
			"invalid repo": {
				200,
				"[{\"name\": \"test-git-repo\", \"format\": \"maven2\", \"type\": \"hosted\"}]",
				200,
				fmt.Errorf("Repository test-git-repo has format 'maven2', format 'raw' requested"),
			},
			"create repo": {
				404,
				"[{\"name\": \"good-repo\", \"format\": \"raw\", \"type\": \"hosted\"}]",
				200,
				nil,
			},
			"get repo failure": {
				401,
				"[{\"name\": \"good-repo\", \"format\": \"raw\", \"type\": \"hosted\"}]",
				200,
				fmt.Errorf("Error response from HTTP call: 401 Unauthorized"),
			},
			"create repo failure": {
				404,
				"[{\"name\": \"good-repo\", \"format\": \"raw\", \"type\": \"hosted\"}]",
				401,
				fmt.Errorf("Error response from HTTP call to build repository: 401 Unauthorized"),
			},
		}
		
	for key, tc := range testCases {
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if strings.HasSuffix(fmt.Sprint(r.URL), "/hosted") {
				w.WriteHeader(tc.createRepoRc)
			} else {
				w.WriteHeader(tc.getRepoRc)
				w.Write([]byte(tc.getRepoJson))
			}
		}))
		defer server.Close()
	 
		dist := gitDist{*NewBaseDist("git", testConfig("nexus", server.URL), "basedirectory")}

		testNexus := nexus.Server{Url: server.URL}
	
		err := dist.CheckNexusRepository(testNexus, "test-git-repo")

		assert.Equal(t, fmt.Sprint(tc.expected), fmt.Sprint(err), "Error mis-match, Test Case: " + key)
	}
}

func TestGitInstallLocal(t *testing.T) {
	placeholderComp := testComponent("testPurl")
	testCases := map[string]struct {
		config    *configs.DropprConfig
		testCmd   string
		copy_result error
		expected  Result
		}{
			"good": {
				testConfig("localinstall", ""),
				"",
				nil,
				Result{true, placeholderComp, ""},
			},
			"config repo error": {
				&configs.DropprConfig{},
				"echo",
				nil,
				Result{false, placeholderComp, "Error installing pkg:git/name/space/TestGit@1.2.3 component to local file system: Unable to locate configuration for Purl type git matching repository http://my-repo"},
			},
			"copy error": {
				testConfig("localinstall", ""),
				"echo",
				fmt.Errorf("Mock Copy Error"),
				Result{false, placeholderComp, "Unable to copy from basedirectory/the/collection/dir/TestGit to {TEMPDIR}/the/collection/dir/TestGit, Mock Copy Error."},
			},
			"install failure": {
				testConfig("localinstall", ""),
				"ls",
				nil,
				Result{false, placeholderComp, "exit status 2: ls: cannot access 'init': No such file or directory"},
			},
		}
		
	for key, tc := range testCases {		
		tempDir, _ := os.MkdirTemp("/tmp", "git_test")
		defer os.RemoveAll(tempDir)

		for idx, _ := range tc.config.Repos {
			tc.config.Repos[idx].Package_Manager_Command = []string{tc.testCmd}
			tc.config.Repos[idx].Target_Location = tempDir
		}
	
		dist := gitDist{*NewBaseDist("git", tc.config, "basedirectory")}
		comp := testComponent("pkg:git/name/space/TestGit@1.2.3")
		
		mockCopy := func(string, tgt string) error {
			os.MkdirAll(tgt, os.ModePerm)
			return tc.copy_result
		}


		result := dist.installLocal(comp, dist.buildCommand, mockCopy)

		assert.Equal(t, tc.expected.Success, result.Success, "Result Status mis-match, Test Case: " + key)
		assert.Equal(t, comp, result.Component, "Component mis-match, Test Case: " + key)
		assert.Equal(t, strings.Replace(tc.expected.Message, "{TEMPDIR}", tempDir, -1), result.Message, "Message mis-match, Test Case: " + key)
	}
}
