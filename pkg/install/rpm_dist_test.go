/*
 *  File: rpm_dist_test.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"bytes"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"testing/fstest"

	"github.com/stretchr/testify/assert"
	"gitlab.com/hoppr/droppr/pkg/configs"
	"gitlab.com/hoppr/droppr/pkg/nexus"
)

func TestRpmInstallNexus(t *testing.T) {
	testCases := map[string]struct {
		fileNames []string
		uploadRc  []int
		openError bool
		expected  error
	}{
		"good": {
			[]string{"file-1.rpm", "file-2.rpm", "file-3.rpm"},
			[]int{200, 200, 200},
			false,
			nil,
		},
		"upload failure": {
			[]string{"file-1.rpm", "file-2.rpm", "file-3.rpm"},
			[]int{200, 200, 400},
			false,
			fmt.Errorf("Error response from upload HTTP call: 400 Bad Request"),
		},
		"read error": {
			[]string{"file-1.rpm", "file-2.rpm", "file-3.rpm"},
			[]int{200, 200, 200},
			true,
			fmt.Errorf("Mock Open Error"),
		},
		"dir read error": {
			[]string{"ERRfile--1.rpm", "ERRfile-2.rpm", "ERRfile-3.rpm"},
			[]int{200, 200, 200},
			false,
			fmt.Errorf("Mock Directory Read Error"),
		},
	}

	for key, tc := range testCases {
		serverCallIndex := 0
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(tc.uploadRc[serverCallIndex])
			serverCallIndex++
		}))

		dist := rpmDist{*NewBaseDist("rpm", testConfig("nexus", server.URL), "basedirectory")}
		comp := testComponent("pkg:rpm/name/space/testrpm@1.2.3")

		testNexus := nexus.Server{Url: server.URL}

		mockFileSys := fstest.MapFS{}
		for _, name := range tc.fileNames {
			if strings.HasSuffix(name, "/") {
				mockFileSys[strings.TrimRight(name, "/")] = &fstest.MapFile{Mode: os.ModeDir}
			} else {
				mockFileSys[name] = &fstest.MapFile{}
			}
		}

		mockReadDir := func(name string) ([]os.DirEntry, error) {
			content := []os.DirEntry{}
			for name, _ := range mockFileSys {
				if strings.HasPrefix(name, "ERR") {
					return content, fmt.Errorf("Mock Directory Read Error")
				}
				fi, _ := mockFileSys.Stat(name)
				content = append(content, fs.FileInfoToDirEntry(fi))
			}
			return content, nil
		}

		mockOpen := func(name string) (io.Reader, error) {
			if tc.openError {
				return bytes.NewReader([]byte("")), fmt.Errorf("Mock Open Error")
			}
			return bytes.NewReader([]byte("Data from file " + name)), nil
		}
		mockTargetDirString := "SomeFilePath"

		err := dist.installNexus(comp, testNexus, "test_rpm_repo", mockReadDir, mockOpen, mockTargetDirString)

		assert.Equal(t, fmt.Sprint(tc.expected), fmt.Sprint(err), "Result Status mis-match, Test Case: "+key)
	}
}

func TestRpmCheckNexusRepository(t *testing.T) {
	testCases := map[string]struct {
		getRepoRc    int
		getRepoJson  string
		createRepoRc int
		expected     error
	}{
		"got repo": {
			200,
			"[{\"name\": \"test-rpm-repo\", \"format\": \"yum\", \"type\": \"hosted\"}]",
			200,
			nil,
		},
		"invalid repo": {
			200,
			"[{\"name\": \"test-rpm-repo\", \"format\": \"pypi\", \"type\": \"hosted\"}]",
			200,
			fmt.Errorf("Repository test-rpm-repo has format 'pypi', format 'yum' requested"),
		},
		"create repo": {
			404,
			"[{\"name\": \"good-repo\", \"format\": \"yum\", \"type\": \"hosted\"}]",
			200,
			nil,
		},
		"get repo failure": {
			401,
			"[{\"name\": \"good-repo\", \"format\": \"yum\", \"type\": \"hosted\"}]",
			200,
			fmt.Errorf("Error response from HTTP call: 401 Unauthorized"),
		},
		"create repo failure": {
			404,
			"[{\"name\": \"good-repo\", \"format\": \"yum\", \"type\": \"hosted\"}]",
			401,
			fmt.Errorf("Error response from HTTP call to build repository: 401 Unauthorized"),
		},
	}

	for key, tc := range testCases {
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if strings.HasSuffix(fmt.Sprint(r.URL), "/hosted") {
				w.WriteHeader(tc.createRepoRc)
			} else {
				w.WriteHeader(tc.getRepoRc)
				w.Write([]byte(tc.getRepoJson))
			}
		}))
		defer server.Close()

		dist := rpmDist{*NewBaseDist("rpm", testConfig("nexus", server.URL), "basedirectory")}

		testNexus := nexus.Server{Url: server.URL}

		testParam := float64(5)

		err := dist.CheckNexusRepository(testNexus, "test-rpm-repo", testParam)

		assert.Equal(t, fmt.Sprint(tc.expected), fmt.Sprint(err), "Error mis-match, Test Case: "+key)
	}
}

func TestRpmCheckNexusGroupRepository(t *testing.T) {
	testCases := map[string]struct {
		getRepoRc         int
		getRepoJson       string
		mock_group_exists bool
		updateGroupRepoRc int
		createGroupRepoRc int
		expected          error
	}{
		"got repo": {
			200,
			"[{\"name\": \"test-rpm-repo\", \"format\": \"yum\", \"type\": \"group\"}]",
			false,
			204,
			200,
			nil,
		},
		"create group repo": {
			404,
			"[{\"name\": \"create-good-repo\", \"format\": \"yum\", \"type\": \"hosted\"}]",
			true,
			200,
			200,
			nil,
		},
		"get repo failure": {
			401,
			"[{\"name\": \"unauthorized\", \"format\": \"yum\", \"type\": \"hosted\"}]",
			false,
			401,
			401,
			fmt.Errorf("Error response from HTTP call: 401 Unauthorized"),
		},
		"create group repo failure": {
			200,
			"[{\"name\": \"group failure\", \"format\": \"yum\", \"type\": \"hosted\"}]",
			false,
			401,
			401,
			fmt.Errorf("Error response from HTTP call to build/update group repository: 401 Unauthorized"),
		},
	}

	mock_group_name := "test-rpm-repo"

	for key, tc := range testCases {
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			if strings.HasSuffix(fmt.Sprint(r.URL), "/group") {
				if tc.mock_group_exists {
					w.WriteHeader(tc.updateGroupRepoRc)
				} else {
					w.WriteHeader(tc.createGroupRepoRc)
				}

			} else {
				w.WriteHeader(tc.getRepoRc)
				w.Write([]byte(tc.getRepoJson))
			}

		}))

		defer server.Close()

		dist := rpmDist{*NewBaseDist("rpm", testConfig("nexus", server.URL), "basedirectory")}
		testNexus := nexus.Server{Url: server.URL}
		err := dist.CheckNexusGroupRepository(testNexus, mock_group_name)
		assert.Equal(t, fmt.Sprint(tc.expected), fmt.Sprint(err), "Error mis-match, Test Case: "+key)
	}
}

func TestRpmInstallLocal(t *testing.T) {
	placeholderComp := testComponent("testPurl")
	testCases := map[string]struct {
		config    *configs.DropprConfig
		testCmd   string
		fileNames []string
		expected  Result
	}{
		"good": {
			testConfig("localinstall", ""),
			"echo",
			[]string{"file1.rpm", "file2.rpm", "file3.rpm"},
			Result{true, placeholderComp, ""},
		},
		"config repo error": {
			&configs.DropprConfig{},
			"echo",
			[]string{"alpha_1.2.3.pom", "beta_4.5.jar", "gamma_6.7.8.jar"},
			Result{false, placeholderComp, "Error installing pkg:rpm/name/space/file1.rpm component locally: Unable to locate configuration for Purl type rpm matching repository http://my-repo"},
		},
		"dir read error": {
			testConfig("localinstall", ""),
			"echo",
			[]string{"alpha_1.2.3.pom", "ERRbeta_4.5.jar", "gamma_6.7.8.jar"},
			Result{false, placeholderComp, "Mock Directory Read Error"},
		},
		"install failure": {
			testConfig("localinstall", ""),
			"ls",
			[]string{"file1.rpm", "file2.rpm", "file3.rpm"},
			Result{false, placeholderComp, "exit status 2: ls: invalid option -- 'y'\nTry 'ls --help' for more information."},
		},
	}

	for key, tc := range testCases {
		for idx, _ := range tc.config.Repos {
			tc.config.Repos[idx].Package_Manager_Command = []string{tc.testCmd}
		}

		dist := rpmDist{*NewBaseDist("rpm", tc.config, "basedirectory")}
		comp := testComponent("pkg:rpm/name/space/file1.rpm")

		mockFileSys := fstest.MapFS{}
		for _, name := range tc.fileNames {
			if strings.HasSuffix(name, "/") {
				mockFileSys[strings.TrimRight(name, "/")] = &fstest.MapFile{Mode: os.ModeDir}
			} else {
				mockFileSys[name] = &fstest.MapFile{}
			}
		}

		mockReadDir := func(name string) ([]os.DirEntry, error) {
			content := []os.DirEntry{}
			for name, _ := range mockFileSys {
				if strings.HasPrefix(name, "ERR") {
					return content, fmt.Errorf("Mock Directory Read Error")
				}
				fi, _ := mockFileSys.Stat(name)
				content = append(content, fs.FileInfoToDirEntry(fi))
			}
			return content, nil
		}

		result := dist.installDirLocal(comp, dist.buildCommand, mockReadDir)
		assert.Equal(t, tc.expected.Success, result.Success, "Result Status mis-match, Test Case: "+key)
		assert.Equal(t, comp, result.Component, "Component mis-match, Test Case: "+key)
		assert.Equal(t, tc.expected.Message, result.Message, "Message mis-match, Test Case: "+key)
	}

}

func TestRpmGetRepoInfo(t *testing.T) {
	testCases := map[string]struct {
		collectiondir string
		expected      yumRepoInfo
	}{
		"good-nexus-repo": {
			"rpm/https%3A%2F%2Fnexus.global.lmco.com%2Frepository%2Fyum-rocky-proxy/8.6/AppStream/x86_64/os/Packages/g",
			yumRepoInfo{float64(4), "8.6/AppStream/x86_64/os/Packages/g"},
		},
		"good-non-nexus-repo": {
			"rpm/http%3A%2F%2Fatl.mirrors.clouvider.net%2Frocky%2F8%2FBaseOS%2Fx86_64%2Fos/Packages/g",
			yumRepoInfo{0, "Packages/g"},
		},
	}

	for key, tc := range testCases {
		result := getRepoInfo(tc.collectiondir)

		assert.Equal(t, tc.expected.repoDepth, result.repoDepth, "Result depth mis-match, Test Case: "+key)
		assert.Equal(t, tc.expected.targetDir, result.targetDir, "Target directory info mis-match, Test Case: "+key)
	}

}

func TestRpmAddRepoToGroup(t *testing.T) {
	testCases := map[string]struct {
		repoName string
		expected bool
	}{
		"add-to-repo": {
			"some-repo1",
			true,
		},
		"already-exists": {
			"some-repo2",
			false,
		},
	}
	groupRepoMembers = []string{"some-repo2", "another-repo2"}

	for key, tc := range testCases {
		result := addRepoToGroup(tc.repoName)

		assert.Equal(t, tc.expected, result, "Expected and result mis-match, Test Case: "+key)
	}

}
