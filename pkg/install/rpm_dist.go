/*
 *  File: rpm_dist.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"regexp"
	"strings"

	cdx "github.com/CycloneDX/cyclonedx-go"

	"gitlab.com/hoppr/droppr/pkg/nexus"
	"gitlab.com/hoppr/droppr/pkg/utils"
)

type rpmDist struct {
	baseDist
}

type yumRepoInfo struct {
	repoDepth float64
	targetDir string
}

var groupRepoMembers []string

func (self *rpmDist) InstallLocal(comp cdx.Component) Result {
	return self.baseDist.installDirLocal(comp, self.buildCommand, os.ReadDir)
}

func (self *rpmDist) buildCommand(fqfn string, comp cdx.Component) ([]string, error) {
	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		return nil, fmt.Errorf("Error installing %s component locally: %s", comp.PackageURL, err)
	}
	if len(repoConfig.Package_Manager_Command) == 0 || repoConfig.Package_Manager_Command[0] == "" {
		repoConfig.Package_Manager_Command = []string{"dnf"}
	}

	sudopath, err := exec.LookPath("sudo")
	command := []string{}

	if err == nil {
		self.Log().Printf("Prepending sudo to command as 'sudo' found in '%s'\n", sudopath)
		command = append(command, "sudo")
	}
	command = append(command, repoConfig.Package_Manager_Command...)
	command = append(command, "install", "-y", fqfn)
	return command, nil
}

func (self *rpmDist) InstallNexus(comp cdx.Component) Result {

	// Nexus install involves the following steps
	// 1. For each component to be uploaded to Nexus, the target repo with the right yum depth needs to be determind.
	// The helper function getRepoInfo determines the name, depth and directory structure of the target repository
	// 2. CheckNexusRepository creates a new repository if it has not been already created
	// 3. After the files get successfully uploaded, the yum folders with different data depths must be grouped together.
	// CheckNexusGroupRepository creates a new group /adds the repositories to the group

	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		msg := fmt.Sprintf("Error installing %s component to Nexus: %s", comp.PackageURL, err)
		self.Log().Errorf(msg)
		return Result{false, comp, msg}
	}

	serverUrl, repoName := nexus.ParseLocation(repoConfig.Target_Location, "droppr_rpm")
	collectiondir := getCollectionData(comp)[collectionDirectory]
	targetRepoInfo := getRepoInfo(collectiondir)
	repoNamewithDepth := repoName + "--d" + fmt.Sprint(targetRepoInfo.repoDepth)

	targetNexus := nexus.Server{
		Url:      serverUrl,
		Username: repoConfig.Username,
		Password: repoConfig.Password,
	}

	err = self.CheckNexusRepository(targetNexus, repoNamewithDepth, targetRepoInfo.repoDepth)
	if err != nil {
		return Result{false, comp, err.Error()}
	}
	err = self.installNexus(comp, targetNexus, repoNamewithDepth, os.ReadDir, utils.OpenFileAsReader, targetRepoInfo.targetDir)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	repoAddedToGroup := addRepoToGroup(repoNamewithDepth)
	if repoAddedToGroup {
		err = self.CheckNexusGroupRepository(targetNexus, repoName)
		if err != nil {
			return Result{false, comp, err.Error()}
		}
	}

	return Result{true, comp, ""}

}

func addRepoToGroup(
	repoNamewithDepth string) bool {
	for _, elem := range groupRepoMembers {
		if elem == repoNamewithDepth {
			return false
		}
	}
	groupRepoMembers = append(groupRepoMembers, repoNamewithDepth)
	return true

}

func (self *rpmDist) installNexus(
	comp cdx.Component,
	targetNexus nexus.Server,
	repoName string,
	readDirFunc func(name string) ([]os.DirEntry, error),
	openFunc func(name string) (io.Reader, error),
	tgt string,
) error {

	dir := self.baseDir + string(os.PathSeparator) + getCollectionData(comp)[collectionDirectory]

	files, err := readDirFunc(dir)
	if err != nil {
		return err
	}

	for _, f := range files {
		fptr, err := openFunc(dir + string(os.PathSeparator) + f.Name())
		if err != nil {
			return err
		}

		data := map[string]interface{}{
			"yum.asset.filename": f.Name(),
			"yum.directory":      tgt,
		}

		err = targetNexus.Upload(f.Name(), "yum.asset", fptr, repoName, data)
		if err != nil {
			return err
		}
	}

	return nil
}

func getRepoInfo(collectiondir string) yumRepoInfo {
	//This helper function is used to determine the yum repo's depth,name and target directory

	// Check the output of the hoppr:collection:directory
	// Here are some of the examples
	// "rpm/http%3A%2F%2Fatl.mirrors.clouvider.net%2Frocky%2F8%2FBaseOS%2Fx86_64%2Fos/Packages/g"
	// "rpm/https%3A%2F%2Fnexus.global.lmco.com%2Frepository%2Fyum-rocky-proxy/8.6/AppStream/x86_64/os/Packages/g"

	rerepo := regexp.MustCompile(`^rpm/.*?/(.*)$`)
	matches := rerepo.FindStringSubmatch(collectiondir)

	// Split the string based on %2F or / because the input string can have both formats
	combinedFolderInfo := matches[len(matches)-1]
	folders := regexp.MustCompile(`%2F|\/`).Split(combinedFolderInfo, -1)
	depth := len(folders)
	yumInfo := &yumRepoInfo{}

	for d, folder := range folders {
		if folder == "Packages" || folder == "RPMS" {
			depth = d
		}
	}

	yumInfo.repoDepth = float64(depth)
	yumInfo.targetDir = strings.Join(folders[:], "/")
	return *yumInfo
}

func (self *rpmDist) CheckNexusRepository(targetNexus nexus.Server, repoName string, depth float64) error {
	targetNexus.GetLock().Lock()
	defer targetNexus.GetLock().Unlock()

	repo, err := targetNexus.GetRepository(repoName)

	if err != nil {
		return err
	}

	additionalParams := map[string]interface{}{
		"component": map[string]bool{
			"proprietaryComponents": true,
		},
		"yum": map[string]interface{}{
			"repodataDepth": depth,
			"deployPolicy":  "STRICT",
		},
	}

	if repo != nil {
		err := self.ValidateNexusRepository(repo, "yum", additionalParams)
		if err != nil {
			return err
		}
	} else {

		err = targetNexus.CreateRepository(repoName, "yum", "yum", additionalParams)
		if err != nil {
			return err
		}
	}

	return nil
}

func (self *rpmDist) CheckNexusGroupRepository(targetNexus nexus.Server, groupName string) error {
	targetNexus.GetLock().Lock()
	defer targetNexus.GetLock().Unlock()
	group, err := targetNexus.GetRepository(groupName)
	if err != nil {
		return err
	}

	additionalParams := map[string]interface{}{
		"group": map[string]interface{}{
			"memberNames": groupRepoMembers,
		},
	}

	if group != nil {
		err = targetNexus.CreateGroupRepository(groupName, "yum", "yum", additionalParams, false)

	} else {
		err = targetNexus.CreateGroupRepository(groupName, "yum", "yum", additionalParams, true)
	}
	if err != nil {
		return err
	}

	return nil
}
