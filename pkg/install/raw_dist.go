/*
 *  File: raw_dist.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"fmt"
	"io"
	"os"

	cdx "github.com/CycloneDX/cyclonedx-go"

	"gitlab.com/hoppr/droppr/pkg/nexus"
	"gitlab.com/hoppr/droppr/pkg/utils"
)

type rawDist struct {
	baseDist
}

func (self *rawDist) InstallNexus(comp cdx.Component) Result {
	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		msg := fmt.Sprintf("Error installing %s component to Nexus: %s", comp.PackageURL, err)
		self.Log().Error(msg)
		return Result{false, comp, msg}
	}

	serverUrl, repoName := nexus.ParseLocation(repoConfig.Target_Location, "droppr_raw")

	targetNexus := nexus.Server{
		Url: serverUrl, 
		Username: repoConfig.Username,
		Password: repoConfig.Password,
	}

	err = self.CheckNexusRepository(targetNexus, repoName)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	return self.installNexus(comp, targetNexus, repoName, utils.OpenFileAsReader)
}

func (self *rawDist) installNexus(
	comp cdx.Component,
	targetNexus nexus.Server,
	repoName string,
	openFunc func(name string) (io.Reader, error),
) Result {

	compPurl := ParsePurl(comp.PackageURL)
	fileName := compPurl.Name
	directoryName := string(os.PathSeparator) +  compPurl.Namespace

	data := map[string]interface{}{}

	sourceDir := self.baseDir + string(os.PathSeparator) + getCollectionData(comp)[collectionDirectory]

	data["raw.directory"] = directoryName
	data["raw.asset1.filename"] = fileName

	fptr, err := openFunc(sourceDir + string(os.PathSeparator) + fileName)
	if err != nil {
		return Result{false, comp, err.Error()}
	}
	err = targetNexus.Upload(fileName, "raw.asset1", fptr, repoName, data)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	return Result{true, comp, ""}
}

func (self *rawDist) CheckNexusRepository(targetNexus nexus.Server, repoName string) error {
	targetNexus.GetLock().Lock()
	defer targetNexus.GetLock().Unlock()

	repo, err := targetNexus.GetRepository(repoName)
	if err != nil {
		return err
	}

	additionalParams := map[string]interface{}{
		"component": map[string]bool{
			"proprietaryComponents": true,
		},
	}

	if repo != nil {
		err := self.ValidateNexusRepository(repo, "raw", additionalParams)
		if err != nil {
			return err
		}
	} else {
		err := targetNexus.CreateRepository(repoName, "raw", "raw", additionalParams)
		if err != nil {
			return err
		}
	}

	return nil
}
