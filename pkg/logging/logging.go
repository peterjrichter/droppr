/* 
 *  File: logging.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *  
 *  MIT License
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 */
 
 package logging

import (
	"bytes"
	"fmt"
	"io"
	"strings"

	log "github.com/sirupsen/logrus"
)

var logOutFile io.Writer

type LogFormatter struct {
	log.TextFormatter
	id string
}

func (f *LogFormatter) Format(entry *log.Entry) ([]byte, error) {
	idstring := ""
	if f.id != "" {
		idstring = "[" + strings.ToUpper(f.id) + "]"
	}
	return []byte(fmt.Sprintf("%s [%s]%s %s\n", entry.Time.Format("2006-01-02 15:04:05.000"), strings.ToUpper(entry.Level.String()), idstring, entry.Message)), nil
}

func Initialize(logFile io.Writer) {
	logOutFile = logFile
	log.SetOutput(logFile)
	log.SetFormatter(&LogFormatter{log.TextFormatter{}, ""})
}

type MemoryLogger struct {
	log.Logger
	buffer *bytes.Buffer
}

func NewMemoryLogger(id string) *MemoryLogger {
	m := MemoryLogger{*log.New(), nil}
	m.buffer = new(bytes.Buffer)
	m.SetOutput(m.buffer)
	m.SetFormatter(&LogFormatter{log.TextFormatter{}, id})

	return &m
}

func (self *MemoryLogger) Flush() {
	if self != nil && self.buffer != nil && logOutFile != nil {
		io.WriteString(logOutFile, self.buffer.String())
		self.buffer.Reset()
	}
}
