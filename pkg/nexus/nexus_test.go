package nexus

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetRepository(t *testing.T) {
	testCases := map[string]struct {
		repoName      string
		statusCode    int
		body          string
		expectedRepo  *Repository
		expectedError error
	}{
		"good": {
			"good-repo",
			200,
			"[{\"name\": \"good-repo\", \"format\": \"pypi\", \"type\": \"hosted\"}]",
			&Repository{
				Name:   "good-repo",
				Format: "pypi",
				Type:   "hosted"},
			nil,
		},
		"not-found":    {"trash", 200, "[]", nil, nil},
		"Error return": {"trash", 500, "Error", nil, fmt.Errorf("Error response from HTTP call: 500 Internal Server Error")},
		"bad-json": {
			"bad-json",
			200,
			"ERR: Mock Read Error",
			nil,
			fmt.Errorf("invalid character 'E' looking for beginning of value"),
		},
	}

	for key, tc := range testCases {
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(tc.statusCode)
			w.Write([]byte(tc.body))
		}))
		defer server.Close()

		testNexus := Server{Url: server.URL}

		repo, err := testNexus.GetRepository(tc.repoName)

		assert.Equal(t, tc.expectedRepo, repo, "Repo mis-match, Test Case: "+key)
		assert.Equal(t, strings.Replace(fmt.Sprint(tc.expectedError), "$URL$", server.URL, -1), fmt.Sprint(err), "Error mis-match, Test Case: "+key)
	}
}

func TestCreateRepository(t *testing.T) {
	testCases := map[string]struct {
		repoType   string
		statusCode int
		expected   error
	}{
		"good":          {"testType", 200, nil},
		"Error return":  {"testType", 500, fmt.Errorf("Error response from HTTP call to build repository: 500 Internal Server Error")},
		"bad-repo-type": {"bad%Type", 200, fmt.Errorf("parse \"$URL$/service/rest/v1/repositories/bad%%Type/hosted\": invalid URL escape \"%%Ty\"")},
	}

	additionalParams := map[string]interface{}{
		"alpha": 42,
		"beta":  "word",
	}

	for key, tc := range testCases {
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(tc.statusCode)
		}))
		defer server.Close()

		testNexus := Server{Url: server.URL}

		err := testNexus.CreateRepository("testRepo", tc.repoType, "testFormat", additionalParams)

		assert.Equal(t, strings.Replace(fmt.Sprint(tc.expected), "$URL$", server.URL, -1), fmt.Sprint(err), "Error mis-match, Test Case: "+key)
	}
}

func TestCreateGroupRepository(t *testing.T) {
	testCases := map[string]struct {
		repoType    string
		statusCode  int
		updateGroup bool
		expected    error
	}{
		"good":          {"testType", 200, false, nil},
		"good-update":   {"testType", 200, true, nil},
		"Error return":  {"testType", 500, false, fmt.Errorf("Error response from HTTP call to build/update group repository: 500 Internal Server Error")},
		"bad-repo-type": {"bad%Type", 200, false, fmt.Errorf("parse \"$URL$/service/rest/v1/repositories/bad%%Type/group/testRepo\": invalid URL escape \"%%Ty\"")},
	}

	additionalParams := map[string]interface{}{
		"alpha": 42,
		"beta":  "word",
	}

	for key, tc := range testCases {
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(tc.statusCode)
		}))
		defer server.Close()

		testNexus := Server{Url: server.URL}

		err := testNexus.CreateGroupRepository("testRepo", tc.repoType, "testFormat", additionalParams, tc.updateGroup)

		assert.Equal(t, strings.Replace(fmt.Sprint(tc.expected), "$URL$", server.URL, -1), fmt.Sprint(err), "Error mis-match, Test Case: "+key)
	}
}

func TestUpload(t *testing.T) {
	testCases := map[string]struct {
		repoName   string
		statusCode int
		expected   error
	}{
		"good":         {"testRepo", 200, nil},
		"Error return": {"testRepo", 500, fmt.Errorf("Error response from upload HTTP call: 500 Internal Server Error")},
	}

	additionalParams := map[string]interface{}{
		"alpha": "42",
		"beta":  "word",
	}

	for key, tc := range testCases {
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(tc.statusCode)
		}))
		defer server.Close()

		testNexus := Server{Url: server.URL}
		fileContent := []byte("mock file content")
		reader := bytes.NewReader(fileContent)

		err := testNexus.Upload(tc.repoName, "testParam", reader, "testRepo", additionalParams)

		assert.Equal(t, strings.Replace(fmt.Sprint(tc.expected), "$URL$", server.URL, -1), fmt.Sprint(err), "Error mis-match, Test Case: "+key)
	}
}

func TestGetLock(t *testing.T) {
	testCases := map[string]struct {
		urls              []string
		expectedLockCount int
	}{
		"good": {
			[]string{"http://mynexus.com:100", "https://mynexus.com:200", "http://mynexus.com:100/other_stuff"},
			2,
		},
		"panic": {
			[]string{"////"},
			0,
		},
	}

	for key, tc := range testCases {
		for k := range lockMap {
			delete(lockMap, k)
		}

		for _, url := range tc.urls {
			testNexus := Server{Url: url}
			if strings.HasPrefix(key, "panic") {
				assert.Panics(t, func() { testNexus.GetLock() })
			} else {
				testNexus.GetLock()
				testNexus.GetLock() // twice to make sure it doesn't re-create
			}
		}
		assert.Equal(t, tc.expectedLockCount, len(lockMap), "Error, Test Case: "+key)
	}
}
func TestSetRealm(t *testing.T) {
	testCases := map[string]struct {
		getRealmsRc   int
		getRealmsList string
		setRealmsRc   int
		expectedError error
	}{
		"add realm success": {
			200,
			"[\"RealmA\", \"RealmB\"]",
			200,
			nil,
		},
		"realm exists": {
			200,
			"[\"RealmA\", \"TestRealm\", \"RealmB\"]",
			200,
			nil,
		},
		"get realms failure": {
			404,
			"[\"RealmA\", \"RealmB\"]",
			200,
			fmt.Errorf("Error response from get realms HTTP call: 404 Not Found"),
		},
		"add realm failure": {
			200,
			"[\"RealmA\", \"RealmB\"]",
			404,
			fmt.Errorf("Error response from set realms HTTP call: 404 Not Found"),
		},
	}

	for key, tc := range testCases {
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.Method == "GET" {
				w.WriteHeader(tc.getRealmsRc)
				w.Write([]byte(tc.getRealmsList))
			} else {
				w.WriteHeader(tc.setRealmsRc)
			}
		}))
		defer server.Close()

		testNexus := Server{Url: server.URL}

		err := testNexus.SetRealm("TestRealm")

		assert.Equal(t, fmt.Sprint(tc.expectedError), fmt.Sprint(err), "Error mis-match, Test Case: "+key)
	}
}

func TestGetDockerRepo(t *testing.T) {
	testCases := map[string]struct {
		nexusUrl    string
		dockerPort  int
		dockerUrl   string
		expectedUrl string
	}{
		"success": {"http://mynexus.com:8081", 5000, "", "mynexus.com:5000"},
		"url supplied": {"http://mynexus.com:8081", 5000, "https://my.docker.com", "my.docker.com"},
		"no port": {"http://mynexus.com", 5000, "", "mynexus.com:5000"},
		"panic":   {"", 5000, "", "mynexus.com:5000"},
		"panic url supplied":   {"", 5000, "http://", "mynexus.com:5000"},
	}

	for key, tc := range testCases {
		testNexus := Server{Url: tc.nexusUrl, Docker_Url: tc.dockerUrl, Docker_Port: tc.dockerPort}

		if strings.HasPrefix(key, "panic") {
			assert.Panics(t, func() { testNexus.GetDockerRepo() })
		} else {
			actualUrl := testNexus.GetDockerRepo()
			assert.Equal(t, tc.expectedUrl, actualUrl, "Error, Test Case: "+key)
		}
	}
}

func TestParseLocation(t *testing.T) {
	testCases := map[string]struct {
		location     string
		expectedUrl  string
		expectedRepo string
	}{
		"no-repo":         {"http://mynexus.com:8081", "http://mynexus.com:8081", "defaultRepo"},
		"repo-given":      {"http://mynexus.com:8081/repository/somerepo", "http://mynexus.com:8081", "somerepo"},
		"repo-given+more": {"http://mynexus.com:8081/repository/somerepo/garbage", "http://mynexus.com:8081", "somerepo"},
		"no-schema":       {"mynexus.com/repository/somerepo/garbage", "mynexus.com", "somerepo"},
	}

	for key, tc := range testCases {
		actualUrl, actualRepo := ParseLocation(tc.location, "defaultRepo")
		assert.Equal(t, tc.expectedUrl, actualUrl, "URL Error, Test Case: "+key)
		assert.Equal(t, tc.expectedRepo, actualRepo, "Repo Error, Test Case: "+key)
	}

}
