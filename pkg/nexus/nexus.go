/*
 *  File: nexus.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package nexus

// NOTE FOR TESTING:
//   Use httptest package built into go,
//   see https://medium.com/zus-health/mocking-outbound-http-requests-in-go-youre-probably-doing-it-wrong-60373a38d2aa

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"regexp"
	"strings"
	"sync"

	"gitlab.com/hoppr/droppr/pkg/utils"
)

type Server struct {
	Url         string
	Username    string
	Password    string
	Docker_Port int
	Docker_Url	string
	lock        *sync.Mutex
}

type Repository struct {
	Name       string                 `json:"name"`
	Format     string                 `json:"format"`
	Type       string                 `json:"type"`
	Url        string                 `json:"url"`
	Attributes map[string]interface{} `json:"attributes,omitempty"`
	Docker     map[string]interface{} `json:"docker,omitempty"`
	Yum        map[string]interface{} `json:"yum,omitempty"`
}

var lockMap = map[string]*sync.Mutex{}
var lockMapLock sync.Mutex

func ParseLocation(loc string, defaultRepoName string) (string, string) {
	// If a location looks like it specifies a Nexus reposistory, parse out the
	// API url and the repository name.
	//
	// If not, just return the location and the default repository name

	re := regexp.MustCompile(`^((?:.*\://)?[^/]*)/repository/([^/]*)`)
	matches := re.FindStringSubmatch(loc)

	if len(matches) == 3 {
		return matches[1], matches[2]
	} else {
		return loc, defaultRepoName
	}
}

func (self *Server) GetRepository(name string) (*Repository, error) {
	request, err := http.NewRequest(http.MethodGet, strings.TrimRight(self.Url, "/ ")+"/service/rest/v1/repositorySettings", nil)
	if err != nil {
		return nil, err
	}

	request.Header.Add("Accept", "application/json")
	request.SetBasicAuth(self.Username, self.Password)
	client := &http.Client{}

	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}
	if response.StatusCode == 404 {
		return nil, nil
	}
	if response.StatusCode >= 300 {
		err = fmt.Errorf("Error response from HTTP call: %s", response.Status)
		return nil, err
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	repos := []Repository{}
	err = json.Unmarshal(responseData, &repos)
	if err != nil {
		return nil, err
	}

	for _, repo := range repos {
		if repo.Name == name {
			return &repo, nil
		}
	}

	return nil, nil
}

func (self *Server) CreateRepository(name string, repoType string, format string, additionalParams map[string]interface{}) error {
	service := "/service/rest/v1/repositories/" + repoType + "/hosted"

	repoRequest := map[string]interface{}{
		"name":   name,
		"online": true,
		"storage": map[string]interface{}{
			"blobStoreName":               "default",
			"strictContentTypeValidation": true,
			"writePolicy":                 "allow",
		},
		"cleanup": map[string]interface{}{"policyNames": []string{}},
	}

	utils.MergeMap(repoRequest, additionalParams)

	requestBody, err := json.Marshal(repoRequest)
	if err != nil {
		fmt.Println("Error On Marshall")
		return err
	}

	reqReader := bytes.NewReader(requestBody)

	request, err := http.NewRequest(http.MethodPost, strings.TrimRight(self.Url, "/ ")+service, reqReader)
	if err != nil {
		fmt.Println("Error On NewRequest")
		return err
	}

	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Accept", "application/json")
	request.SetBasicAuth(self.Username, self.Password)
	client := &http.Client{}

	response, err := client.Do(request)
	if err != nil {
		fmt.Println("Error On Do")
		return err
	}
	if response.StatusCode >= 300 {
		err := fmt.Errorf("Error response from HTTP call to build repository: %s", response.Status)
		return err
	}

	return nil
}

func (self *Server) CreateGroupRepository(groupName string, repoType string, format string, additionalParams map[string]interface{}, createNew bool) error {
	service := "/service/rest/v1/repositories/" + repoType + "/group"

	repoRequest := map[string]interface{}{
		"name":   groupName,
		"online": true,
		"storage": map[string]interface{}{
			"blobStoreName":               "default",
			"strictContentTypeValidation": true,
		},
	}
	for k, v := range additionalParams {
		repoRequest[k] = v
	}

	requestBody, err := json.Marshal(repoRequest)
	if err != nil {
		fmt.Println("Error On Marshall")
		return err
	}

	reqReader := bytes.NewReader(requestBody)
	var request *http.Request
	if createNew {
		request, err = http.NewRequest(http.MethodPost, strings.TrimRight(self.Url, "/ ")+service, reqReader)
	} else {
		service = service + "/" + groupName
		request, err = http.NewRequest(http.MethodPut, strings.TrimRight(self.Url, "/ ")+service, reqReader)
	}

	if err != nil {
		fmt.Println("Error On NewRequest")
		return err
	}

	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Accept", "application/json")
	request.SetBasicAuth(self.Username, self.Password)
	client := &http.Client{}

	response, err := client.Do(request)
	if err != nil {
		fmt.Println("Error On Do")
		return err
	}

	if response.StatusCode >= 300 {
		err := fmt.Errorf("Error response from HTTP call to build/update group repository: %s", response.Status)
		return err
	}

	return nil
}

func (self *Server) Upload(fileName string, paramName string, rdr io.Reader, repo string, data map[string]interface{}) error {
	service := "/service/rest/v1/components?repository=" + repo

	requestBody := &bytes.Buffer{}
	writer := multipart.NewWriter(requestBody)

	part, err := writer.CreateFormFile(paramName, fileName)
	if err != nil {
		return err
	}

	if _, err := io.Copy(part, rdr); err != nil {
		return err
	}

	for key, val := range data {
		_ = writer.WriteField(key, fmt.Sprintf("%v", val))
	}

	if err := writer.Close(); err != nil {
		return err
	}

	request, err := http.NewRequest(http.MethodPost, strings.TrimRight(self.Url, "/ ")+service, requestBody)
	if err != nil {
		fmt.Println("Error On NewRequest")
		return err
	}

	request.Header.Set("Content-Type", writer.FormDataContentType())
	request.SetBasicAuth(self.Username, self.Password)
	client := &http.Client{}

	response, err := client.Do(request)
	if err != nil {
		fmt.Println("Error On Do")
		return err
	}

	if response.StatusCode >= 300 {
		err = fmt.Errorf("Error response from upload HTTP call: %s", response.Status)
		return err
	}

	return nil
}

func (self *Server) GetLock() *sync.Mutex {
	if self.lock != nil {
		return self.lock
	}

	re := regexp.MustCompile(`^(?:.*\://)?([^/]*)`)
	matches := re.FindStringSubmatch(self.Url)

	if len(matches) < 2 || matches[1] == "" {
		panic("Unable to extract server/port from " + self.Url)
	}

	host := matches[1]

	lockMapLock.Lock()
	defer lockMapLock.Unlock()

	_, found := lockMap[host]
	if !found {
		lockMap[host] = new(sync.Mutex)
	}
	self.lock = lockMap[host]
	return self.lock
}

func (self *Server) SetRealm(realmToCheck string) error {

	service := "/service/rest/v1/security/realms/active"

	request, err := http.NewRequest(http.MethodGet, strings.TrimRight(self.Url, "/ ")+service, nil)
	if err != nil {
		fmt.Println("Error On NewRequest to collect active realms")
		return err
	}

	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Accept", "application/json")
	request.SetBasicAuth(self.Username, self.Password)
	client := &http.Client{}

	response, err := client.Do(request)
	if err != nil {
		fmt.Println("Error On Do")
		return err
	}

	if response.StatusCode >= 300 {
		err = fmt.Errorf("Error response from get realms HTTP call: %s", response.Status)
		return err
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return err
	}

	realms := []string{}
	err = json.Unmarshal(responseData, &realms)
	if err != nil {
		return err
	}

	for _, r := range realms {
		if r == realmToCheck {
			return nil
		}
	}

	realms = append(realms, realmToCheck)

	requestBody, err := json.Marshal(realms)
	if err != nil {
		return err
	}

	reqReader := bytes.NewReader(requestBody)

	request, err = http.NewRequest(http.MethodPut, strings.TrimRight(self.Url, "/ ")+service, reqReader)
	if err != nil {
		fmt.Println("Error On NewRequest to update active realms")
		return err
	}

	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Accept", "application/json")
	request.SetBasicAuth(self.Username, self.Password)

	response, err = client.Do(request)
	if err != nil {
		fmt.Println("Error On Do")
		return err
	}

	if response.StatusCode >= 300 {
		err = fmt.Errorf("Error response from set realms HTTP call: %s", response.Status)
		return err
	}

	return nil
}

func (self *Server) GetDockerRepo() string {
	if self.Docker_Url != "" {
		re := regexp.MustCompile(`^(?:.*\://)?(.*)`)
		matches := re.FindStringSubmatch(self.Docker_Url)

		if len(matches) < 1 || matches[1] == "" {
			panic("Unable to extract server from " + self.Docker_Url)
		}
	
		return matches[1]
	}
	
	re := regexp.MustCompile(`^(?:.*\://)?([^:/]*)`)
	matches := re.FindStringSubmatch(self.Url)

	if len(matches) < 2 || matches[1] == "" {
		panic("Unable to extract server from " + self.Url)
	}

	return fmt.Sprintf("%s:%d", matches[1], self.Docker_Port)
}
