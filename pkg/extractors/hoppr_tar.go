/* 
 *  File: hoppr_tar.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *  
 *  MIT License
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 */

package extractors

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
)

func UnpackHopprArtifact(outdir string, bundle string) error {
	return unpackHopprArtifact(outdir, bundle, 
		os.Open, ExtractTarGz, ExtractTar)
}

func unpackHopprArtifact(outdir string, bundle string,
	openFunc func(name string) (*os.File, error),
	extractTarGzFunc func(gzipStream io.Reader, outdir string) error,
	extractTarFunc func(uncompressedStream io.Reader, outdir string) error,
	) error {

	tarfile, err := openFunc(bundle)
	defer tarfile.Close()

	if err != nil {
		return fmt.Errorf("Cowardly failed to open artifact: %s", err)
	}

	switch {
	case strings.HasSuffix(bundle, ".tar.gz") || strings.HasSuffix(bundle, ".tgz"):
		return extractTarGzFunc(tarfile, outdir)
	case strings.HasSuffix(bundle, ".tar"):
		return extractTarFunc(tarfile, outdir)
	default:
		return fmt.Errorf("No extraction defined for %s", bundle)
	}
}

func ExtractTarGz(gzipStream io.Reader, outdir string) error {
	return extractTarGz(gzipStream, outdir, gzip.NewReader, ExtractTar)
}

func extractTarGz(gzipStream io.Reader, outdir string,
	newReaderFunc func(r io.Reader) (*gzip.Reader, error),
	extractTarFunc func(uncompressedStream io.Reader, outdir string) error,
	) error {

	uncompressedStream, err := newReaderFunc(gzipStream)
	if err != nil {
		return err
	}

	return extractTarFunc(uncompressedStream, outdir)
}

func ExtractTar(uncompressedStream io.Reader, outdir string) error {
	tarReader := tar.NewReader(uncompressedStream)

	return extractTar(tarReader, outdir, tarReader.Next, os.MkdirAll, os.Create, io.Copy)
}

func extractTar(tarReader *tar.Reader, outdir string,
	readNextFunc func() (*tar.Header, error),
	mkdirFunc func(path string, perm os.FileMode) error,
	createFunc func(name string) (*os.File, error), 
	copyFunc func(dst io.Writer, src io.Reader) (int64, error), 
	) error {

	var header *tar.Header
	var err error

	for header, err = readNextFunc(); err == nil; header, err = readNextFunc() {
		path := filepath.Join(outdir, strings.Replace(header.Name, "./", "", 1))
		switch header.Typeflag {
			case tar.TypeDir:
				if err := mkdirFunc(path, 0755); err != nil {
					return fmt.Errorf("ExtractTar: Mkdir() failed: %w", err)
				}
			case tar.TypeReg:
				outFile, err := createFunc(path)
				if err != nil {
					return fmt.Errorf("ExtractTar: Create() failed: %w", err)
				}

				if _, err := copyFunc(outFile, tarReader); err != nil {
					return fmt.Errorf("ExtractTar: Copy() failed: %w", err)
				}
			default:
				return fmt.Errorf("ExtractTar: uknown type: %b in %s", header.Typeflag, header.Name)
		}
	}
	if err != io.EOF {
		return fmt.Errorf("ExtractTar: Next() failed: %w", err)
	}
	return nil
}
