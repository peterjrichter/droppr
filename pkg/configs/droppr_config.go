/* 
 *  File: droppr_config.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *  
 *  MIT License
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 */

package configs

type DropprConfig struct {
	Repos       []PackageType `yaml:"repos" json:"repos" json:""`
	Num_Workers int           `yaml:"num_workers" json:"num_workers"`
}

type Artifact struct {
	Path string `yaml:"path" json:"path"`
}

type PackageType struct {
	Purl_Type               string   `yaml:"purl_type" json:"purl_type"`
	Target_Type             string   `yaml:"target_type" json:"target_type"`
	Target_Location         string   `yaml:"target_location" json:"target_location"`
	Username                string   `yaml:"username" json:"username"`
	Password                string   `yaml:"-" json:"-"`
	Password_Env            string   `yaml:"password_env" json:"password_env"`
	Regex_Match             string   `yaml:"regex_match,omitempty" json:"regex_match,omitempty"`
	Package_Manager_Command []string `yaml:"package_manager_command,omitempty" json:"package_manager_command,omitempty"`
	Nexus_Docker_Port       int      `yaml:"nexus_docker_port,omitempty" json:"nexus_docker_port,omitempty"`
	Docker_Url              string   `yaml:"docker_url,omitempty" json:"docker_url,omitempty"`
}

func (pkg PackageType) Fill_Defaults(pType string) PackageType {
	// setting default values
	// if no values present
	if pkg.Password == "" {
		pkg.Password = "mypass1"
	}
	if pkg.Password_Env == "" {
		pkg.Password_Env = "MY_PASS_ONE"
	}

	if pkg.Regex_Match == "" {
		pkg.Regex_Match = "/filter.repository.com/.*/"
	}
	if pkg.Target_Type == "" {
		pkg.Target_Type = "nexus"
	}
	if pkg.Target_Location == "" {
		pkg.Target_Location = "nexus.myhost.com"
	}
	if pkg.Username == "" {
		pkg.Username = "test_user"
	}
	if pkg.Nexus_Docker_Port == 0 {
		pkg.Nexus_Docker_Port = 5000
	}
	if pkg.Purl_Type == "" {
		pkg.Purl_Type = pType
	}
	return pkg
}
