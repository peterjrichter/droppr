/*
 *  File: droppr_config.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package configs

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPackageType_Fill_Defaults(t *testing.T) {
	type fields struct {
		Purl_Type       string
		Target_Type     string
		Target_Location string
		Username        string
		Password        string
		Password_Env    string
		Regex_Match     string
	}
	type args struct {
		pType string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   PackageType
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			pkg := PackageType{
				Purl_Type:       tt.fields.Purl_Type,
				Target_Type:     tt.fields.Target_Type,
				Target_Location: tt.fields.Target_Location,
				Username:        tt.fields.Username,
				Password:        tt.fields.Password,
				Password_Env:    tt.fields.Password_Env,
				Regex_Match:     tt.fields.Regex_Match,
			}
			got := pkg.Fill_Defaults(tt.args.pType)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestFillDefaults(t *testing.T) {
	// Test with empty PackageType
	pkg := PackageType{}
	pType := "test"
	pkg = pkg.Fill_Defaults(pType)
	assert.Equal(t, "mypass1", pkg.Password)
	assert.Equal(t, "MY_PASS_ONE", pkg.Password_Env)
	assert.Equal(t, "/filter.repository.com/.*/", pkg.Regex_Match)
	assert.Equal(t, "nexus", pkg.Target_Type)
	assert.Equal(t, "nexus.myhost.com", pkg.Target_Location)
	assert.Equal(t, "test_user", pkg.Username)
	assert.Equal(t, pType, pkg.Purl_Type)

	// Test with non-empty PackageType
	pkg = PackageType{
		Password:        "password",
		Password_Env:    "PASSWORD_ENV",
		Regex_Match:     "/regex/",
		Target_Type:     "target_type",
		Target_Location: "target_location",
		Username:        "username",
		Purl_Type:       "purl_type",
	}
	pType = "test"
	pkg = pkg.Fill_Defaults(pType)
	assert.Equal(t, "password", pkg.Password)
	assert.Equal(t, "PASSWORD_ENV", pkg.Password_Env)
	assert.Equal(t, "/regex/", pkg.Regex_Match)
	assert.Equal(t, "target_type", pkg.Target_Type)
	assert.Equal(t, "target_location", pkg.Target_Location)
	assert.Equal(t, "username", pkg.Username)
	assert.Equal(t, "purl_type", pkg.Purl_Type)
}
