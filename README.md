# Droppr

## Description

Droppr is a command-line tool to facilitate the unpacking of bundles created by [Hoppr](https://hoppr.dev).

Droppr in written in the "go" language, to produce a single executable, simplifying installation and usage.

### Artifact Delivery Options
Artifacts from the Hoppr bundle can be delivered 
- to repositories in a [Nexus](https://www.sonatype.com/products/sonatype-nexus-repository) instance
- As files on the local file system
- Directly installed on the target system (where applicable)

Where a delivery occurs is defined in the config file, and may vary between repos/purl-types.

### Limitations

Droppr currently only supports `tar` and `tar.gz` bundles from Hoppr, and only those built with Hoppr version 1.8.0 and later.

Only the following purl types are currently supported:
- `Docker`
- `Generic`
- `Git`
- `Helm`
- `Maven`
- `Pypi`
- `RPM`

## Installation

Go to [Droppr Releases](https://gitlab.com/hoppr/droppr/-/releases) and download the appropriate binary for your system.

## Usage

### Install command

The droppr install command takes the form of 
```
droppr install [flags]

Flags:
  -b, --bundle string    Hoppr bundle to install (required)
  -h, --help             help for install
  -l, --logfile string   Log file location (default "droppr.log")
      --config string    config file (default is .droppr.yml or $HOME/.config/.droppr.yml)
      --num_workers int  set number of worker threads for processing (default 10)
```
If the `config` file is not specified, the program first looks for a `.droppr.yml` file in the current working directory.  If not found there, it looks in `$HOME/.config/.droppr.yml`.

If `num_workers` (the number of worker threads) is not specified on the command line, the value may be specified in the `config` file.  If not found in either of those locations, a default value of 10 is used.

For example:
```
droppr install --bundle bundle.tar.gz --logfile droppr_log.txt --config droppr_config.yml
```

### Config File Schema

A sample config file:
```
num_workers: 20

repos:
    - purl_type: maven
      target_type: nexus
      target_location: http://127.0.0.1:8081/repository/droppr_demo
      username: admin
      password_env: NEXUS_PW
      
    - purl_type: generic
      target_type: filesys
      target_location: target/directory

    - purl_type: pypi
      regex_match: "pypi.org"
      target_type: local
      package_manager_command: ["python3", "-m", "pip"]

    - purl_type: pypi
      target_type: filesys
      target_location: my/pypi/whls
```
The `num_workers` field sets the number of worker threads to be used by Droppr.  This value can be overridden by the `--num_workers` command line option, and will default to 10 if omitted.

The `repos` field is an array, with one entry per purl type.  Each entry indicates how artifacts of that purl type are to be handled.  The fields within each repo are:
- `purl_type`: The purl type being handled by this repo.  Required

- `regex_match`: A regular expression to check against the _source_ repository (the repository from which the artifact was originally copied) to use this repository.  If omitted, it defaults to an empty string (which will always match).  Note that the repository selected will always be the _first_ successful match for a purl type, if there are multiple options.  Therefore, the default (`regex_match` missing or "") should come _last_.

- `target_type`: Indicates how artifacts of this purl type are to be handled.  Required.  Options are:
	- `filesys`, `filesystem`, `file_sys`, or `file_system`: Copy artifacts to the local directory
	- `nexus`: Install artifacts to a Nexus repository
	- `local`, `localinstall`, or `local_install`: Install the artifacts directly onto the local system

- `target_location`: Where to install the artifacts.  Required for `nexus` and `filesystem` installs, ignored for `local`
   - For `filesystem` installs, the directory into which the artifacts are to be copied.
   - For `nexus` installs, the URL of either the Nexus instance itself (_e.g._ `https://mynexus.com:8081`) or a repository within a Nexus instance (_e.g._ `https://mynexus.com:8081/repository/myrepo`).  If the reposiory is not specified, a repository name will be generated based on the purl type (_e.g._ `droppr_pypi`)

- `package_manager_command`: Command (as an array of strings) used to access the appropriate package manager.  Optional for `local` installs, ingored otherwise.  

    If omitted, a reasonable default command is used (_e.g._ `pip` for pypi artifiacts).  If the pypi installation requires sudo access, and uses pip3 rather than pip, the value would be `["sudo", "pip3"]`.

- `username`: User to be used to access Nexus.  Required for `nexus`, ignored otherwise.

- `password_env`: Environment variable containing the password to be used to access Nexus.  Required for `nexus`, ignored otherwise.

- `nexus_docker_port`: _Nexus installs only._  Specifies the port on the Nexus server for Docker processing.  Defaults to 5000.  Ignored if `docker_url` is specified.

- `docker_url`: _Nexus installs only._  This is the URL to be used for docker interactions with the Nexus repository.  If specified, `nexus_docker_port` is ignored, and the Nexus docker repository will be assumed valid and will not be created.
