# This script supports integration testing of Nexus installs.
#
# Lists the contents of the specified Nexus repository via the Nexus api, eliminating fields likely 
# to change from one run to the next.

REPO=$1
CT=""

# The nexus api returns results in a "paginated" form, so that it may be necessary to 
# call a service multiple times to retrieve all the data

while :
do
    curl -s "http://127.0.0.1:8081/service/rest/v1/components?repository=${REPO}${CT}" -o _api_output.txt

    jq '.items[]' _api_output.txt >> _combined.txt

    CT=`jq -r '(.continuationToken)' _api_output.txt`
  	if [ -z "$CT" -o "$CT" = "null" ]; then
		break
	fi
    CT='&continuationToken='${CT}
done

jq -s 'sort_by(.name) | del(.[].id, .[].assets[].id, .[].assets[].lastModified, .[].assets[].blobCreated, .[].assets[].uploaderIp)' _combined.txt

rm _api_output.txt _combined.txt
