/* 
 *  File: root.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *  
 *  MIT License
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 */

package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	log "github.com/sirupsen/logrus"

	"gitlab.com/hoppr/droppr/pkg/configs"
)

const colorReset = "\033[0m"
const colorRed = "\033[31m"

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "droppr",
	Short: "Droppr: A tool for unbundling hoppr bundles.",
	Long: `Droppr: A tool for unbundling hoppr bundles.
	
Providing a simple mechanism to distribute artifacts to target registries and repositories.
	`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is .droppr.yml or $HOME/.config/.droppr.yml)")
	rootCmd.PersistentFlags().Int("num_workers", 10, "set number of worker threads for processing")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	// rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	rootCmd.PersistentFlags().BoolP("debug", "d", false, "Toggle Debug Logging")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() (*configs.DropprConfig, error) {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".droppr" (without extension).
		viper.AddConfigPath(".")
		viper.AddConfigPath(home + "/.config")
		viper.SetConfigType("yaml")
		viper.SetConfigName(".droppr.yml")
	}

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err != nil {
		log.Print(err)
		return nil, err
	}

	var config = new(configs.DropprConfig)
	viper.Unmarshal(config)

	// Look up password_env values

	env_vars_set := true
	for indx, r := range(config.Repos) {

		if r.Password_Env != "" {
			config.Repos[indx].Password = os.Getenv(r.Password_Env)
			if config.Repos[indx].Password == "" {
				fmt.Printf("%sERROR: Unable to read environment variable %s for %s %s install.%s\n", 
					colorRed, r.Password_Env, r.Purl_Type, r.Target_Type, colorReset)
				env_vars_set = false
			}

			if r.Nexus_Docker_Port == 0 {
				config.Repos[indx].Nexus_Docker_Port = 5000
			}
		}
	}
	
	if !env_vars_set {
		os.Exit(5)
	}

	config.Num_Workers = GetNumWorkers(rootCmd, config)
	return config, nil
}

func GetNumWorkers(cmd *cobra.Command, config *configs.DropprConfig) int {
	num_Workers := config.Num_Workers
	if cmd.Flags().Changed("num_workers") {
		num_Workers, _ = cmd.Flags().GetInt("num_workers")
		log.Printf("Worker thread count set to %d on the command line", num_Workers)
	} else if config != nil && config.Num_Workers != 0 {
		num_Workers = config.Num_Workers
		log.Printf("Workers thread count set to %d, from config file", num_Workers)
	} else {
		num_Workers = 10
		log.Printf("Worker thread count not specified, defaulting to %d", num_Workers)
	}

	if num_Workers <= 0 {
		log.Print("Error: Number of workers cannot be negative or 0")
		fmt.Printf("Error: Number of workers cannot be negative or 0")
		os.Exit(1)
	}

	return num_Workers
}
